﻿using UnityEngine;
using TMPro;
using Assets.Scripts.Models;

public class CopyAndBackgroundController : MonoBehaviour
{
    private ColorUpdater _background;
    private TextMeshPro _copy;
    private string baseCopy;

    [SerializeField]
    public bool CopyNeedsReplace;
    [SerializeField]
    [Range(0,1)]
    public float AlphaBackgroundTo = 1f;

    private void Awake()
    {
        _background = GetComponentInChildren<ColorUpdater>();
        _copy = GetComponentInChildren<TextMeshPro>();

        baseCopy = _copy.text;
    }

    public void UpdateFromModel(CopyAndBackgroudModelDTO model)
    {

        //_background.UpdateColor(AlphaColorTo(model.color));
        _background.UpdateColor(model.color);

        if (model.copy != null && CopyNeedsReplace)
            _copy.text = baseCopy.Replace("{{COPY}}", model.copy.Replace("\\n", "\n"));

        _copy.ForceMeshUpdate();

        _copy.color = model.copyColor ?? _copy.color;
        _copy.faceColor = (Color32)(model.copyColor ?? _copy.color);
        _copy.outlineColor = (Color32)(model.copyColor ?? _copy.color);
    }
}
