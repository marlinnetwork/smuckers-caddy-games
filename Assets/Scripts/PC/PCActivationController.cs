﻿using UnityEngine;
using Assets.Scripts.Models;

public class PCActivationController : MonoBehaviour
{
    private LidController _lidController;
    private FruitsController _fruitsController;
    
    private void Awake()
    {
        _lidController = GetComponentInChildren<LidController>();
        _fruitsController = GetComponentInChildren<FruitsController>();
    }

    public void UpdateModel(GameObject fruit, Color color)
    {
        _lidController.UpdateEffect(color);
        _fruitsController.UpdateSpawnableFruit(fruit, color);
    }


    public void UpdateState(PCStateModel state)
    {
        _lidController.UpdateState(state);

        _fruitsController.UpdateState(state);
    }
}