﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Models;
using Assets.Scripts.Effects;

public class LidController : MonoBehaviour
{
    private BoxCollider boxCollider;
    private GameObject currenteffect;
    private Color currentColor;
    private bool currentState;
    private int numPulses = 0;

    [SerializeField]
    [Range(0f, 5f)]
    public float spawnRate = 0.75f;

    [SerializeField]
    [Range(0f, 5f)]
    public float groupSpawnRate = 2.5f;

    [SerializeField]
    [Range(0, 10)]
    public int pulsesPerGroup = 3;

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        groupSpawnRate += spawnRate;
    }

    private void Start()
    {
        currenteffect = Messages.AppControllerBus.GetEffectResourceByName("pulse");

        boxCollider.enabled = false;
    }

    private void OnMouseUp()
    {
        if (currentState)
        {
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new GameSelectModel());
            Messages.AppControllerBus.SetActivePCState(PCControllerState.NotSelectable);
        }
    }

    public void UpdateEffect(Color color)
    {
        currentColor = color;
    }

    public void UpdateState(PCStateModel model)
    {
        currentState = (model.ControllerState == PCControllerState.Selectable);

        boxCollider.enabled = currentState;

        if (currentState)
            StartCoroutine(SpawnEffect());
    }
    
    IEnumerator SpawnEffect()
    {
        if (numPulses >= pulsesPerGroup) {
            yield return new WaitForSecondsRealtime(groupSpawnRate);
            numPulses = 1;
        }
        else
        {
            yield return new WaitForSecondsRealtime(spawnRate);
            numPulses++;
        }

        Spawn();

        if (currentState)
            StartCoroutine(SpawnEffect());
        else
            StopCoroutine(SpawnEffect());
    }

    private void Spawn()
    {
        GameObject inst = Instantiate(currenteffect, transform);

        inst.GetComponentInChildren<IColor>().SetColor(currentColor);

        IEffect e = inst.GetComponentInChildren<IEffect>();

        e.Activate();
    }
}
