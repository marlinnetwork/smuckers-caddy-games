﻿using UnityEngine;
using Assets.Scripts.Models;

public class PCMessagingController : MonoBehaviour
{
    private GameObject _pcCover;
    private GameObject _speechBubble;
    private GameObject _launchAnyway;
    private CopyAndBackgroundController _pcCoverCBC;
    private CopyAndBackgroundController _speechBubbleCBC;
    private CopyAndBackgroundController _launchAnywayCBC;

    private void Awake()
    {
        _pcCover = transform.Find("PCCover").gameObject;
        _speechBubble = transform.Find("SpeechBubble").gameObject;
        _launchAnyway = transform.Find("LaunchAnyway").gameObject;
        _pcCoverCBC = _pcCover.GetComponentInChildren<CopyAndBackgroundController>();
        _speechBubbleCBC = _speechBubble.GetComponentInChildren<CopyAndBackgroundController>();
        _launchAnywayCBC = _launchAnyway.GetComponentInChildren<CopyAndBackgroundController>();
    }

    private void Start()
    {
        UpdateChildrenEnabled();
    }

    public void UpdateState(Assets.Scripts.Models.PCStateModel state)
    {
        //Removing until necessary.
        //SetPCCoverActive(PCState.Internal[PCStateCreator.PCProperty.Visible] && PCState.Internal[PCStateCreator.PCProperty.TooManyPCs] && !PCState.Internal[PCStateCreator.PCProperty.Active] && !PCState.Internal[PCStateCreator.PCProperty.PlayingGame]);
        //SetSpeechBubbleActive(PCState.Internal[PCStateCreator.PCProperty.Visible] && PCState.Internal[PCStateCreator.PCProperty.LaunchAnyway] && !PCState.Internal[PCStateCreator.PCProperty.TooManyPCs] && PCState.Internal[PCStateCreator.PCProperty.Active] && !PCState.Internal[PCStateCreator.PCProperty.PlayingGame]);
        //SetLauchAnywayActive(PCState.Internal[PCStateCreator.PCProperty.Visible] && PCState.Internal[PCStateCreator.PCProperty.LaunchAnyway] && !PCState.Internal[PCStateCreator.PCProperty.TooManyPCs] && PCState.Internal[PCStateCreator.PCProperty.Active] && !PCState.Internal[PCStateCreator.PCProperty.PlayingGame]);
    }

    public void UpdateModel(CopyAndBackgroundModel model)
    {
        //This stems from custom copy being needed for each cbc.
        CopyAndBackgroudModelDTO dto = new CopyAndBackgroudModelDTO()
        {
            color = model.color,
            copyColor = model.copyColor
        };

        UpdateLaunchAnywayUIInstrutionsForOnClick(model);

        if (model.ApplyTo != null)
        {
            foreach (CopyAndBackgroundType type in model.ApplyTo)
            {
                switch (type)
                {
                    case CopyAndBackgroundType.Cover:
                        _pcCoverCBC.UpdateFromModel(dto);
                        break;
                    case CopyAndBackgroundType.SpeechBubble:
                        dto.copy = model.pcName;

                        _speechBubbleCBC.UpdateFromModel(dto);
                        break;
                    case CopyAndBackgroundType.LaunchAnyway:
                        dto.copy = model.fruitType;

                        _launchAnywayCBC.UpdateFromModel(dto);
                        break;
                }
            }
        }
        else
        {
            _pcCoverCBC.UpdateFromModel(dto);

            dto.copy = model.pcName;
            _speechBubbleCBC.UpdateFromModel(dto);

            dto.copy = model.fruitType;
            _launchAnywayCBC.UpdateFromModel(dto);
        }
    }

    private void UpdateLaunchAnywayUIInstrutionsForOnClick(CopyAndBackgroundModel model)
    {
        LaunchAnywayButton _afterClickLaunchAnywayInstructions = _launchAnyway.GetComponentInChildren<LaunchAnywayButton>();

        if(_afterClickLaunchAnywayInstructions != null)
            _afterClickLaunchAnywayInstructions.SetLaunchAnywayProductType(model.productType);
    }

    private void SetPCCoverActive(bool status)
    {
        _pcCover.gameObject.SetActive(status);
    }

    private void SetSpeechBubbleActive(bool status)
    {
        _speechBubble.gameObject.SetActive(status);
    }

    private void SetLauchAnywayActive(bool status)
    {
        _launchAnyway.gameObject.SetActive(status);
    }

    private void UpdateChildrenEnabled(bool cover = false, bool speechBubble = false, bool launchAnyway = false)
    {
        SetPCCoverActive(cover);
        SetSpeechBubbleActive(speechBubble);
        SetLauchAnywayActive(launchAnyway);
    }
}

//Original Testing scenarios
//CopyAndBackgroundController Testing
//UpdatePCCover(_coverColor, _coverCopy, _coverCopyColor);
//UpdateSpeechBubble(_speechBubbleColor, _speechBubbleCopy, _speechBubbleCopyColor);
//UpdateLaunchAnyway(_launchAnywayColor, _launchAnywayCopy, _launchAnywayCopyColor);

//rotational testing use orienter
//_pcCover.GetComponent<Orienter>().UpdateRotations(new Vector3(0,0,180));
//_pcCover.GetComponent<Rotator>().SetNextRotationAndTime(Quaternion.Euler(0, 0, 90), 2f);
//_pcCover.GetComponent<Orienter>().SetNextRotationAndTime(Quaternion.Euler(0, 0, 270), .5f);