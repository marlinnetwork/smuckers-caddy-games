﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Models;
using UnityEngine.Analytics;

public partial class PCController : MonoBehaviour
{
    [SerializeField]
    public FruitType fruitType;

    [SerializeField]
    public ProductType productType;

    [SerializeField]
    public Color BackgroundColor;

    [SerializeField]
    public Color CopyColor;

    [SerializeField]
    public ProductionType productionType;

    //This is set on found and lost only and should be used for new states when set out side of vuforia

    private bool _isVisible { get; set; }
    private PCStateModel _pcState;
    
    private PCActivationController _pcActivationController;
    
    private void Awake()
    {
        _pcActivationController = GetComponentInChildren<PCActivationController>();
        _isVisible = false;
        _pcState = new PCStateModel();
    }

    private void Start()
    {
        _pcActivationController.UpdateModel(Messages.AppControllerBus.GetFruitResourceByType(fruitType), BackgroundColor);
    }

    public void Found()
    {
        _isVisible = true;

        if (_pcState.ControllerState == PCControllerState.InActivate)
        {
            //Add a handle for app controller for this pc
            Messages.AppControllerBus.EnqueueActivePCQueue(this);

            //Veryify it can be active, and set up its state
            ReceiveNewState(Messages.AppControllerBus.CanIBeActivePC(this));
            
            //Removing the activations delay same as above but was used when multi pcs could be active at once.
            //StopCoroutine(AreWeReady());

            //StartCoroutine(AreWeReady());
        }

        if (_pcState.ControllerState == PCControllerState.NotVisiblePlayingGame)
        {
            ReceiveNewState(PCControllerState.PlayingGame);
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.SetBackgroundCopy, Messages.GamesControllerBus.GetActiveGameInstructions());
        }
    }

    public void Lost()
    {
        _isVisible = false;

        switch(_pcState.ControllerState)
        {
            case PCControllerState.PlayingGame:
                Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.SetBackgroundCopy, Instructions.MoveIsPlayingPCBackIntoView.Replace("{{COPY}}", fruitType.ToString()));
                ReceiveNewState(PCControllerState.NotVisiblePlayingGame);
                break;
            case PCControllerState.NotSelectable:
                //if we need to send a message or do something here we can
                break;
            case PCControllerState.Selectable:
            case PCControllerState.InActivate:
            default:
                Messages.AppControllerBus.DequeueActivePCQueue(this);
                break;
        }
    }

    public void ReceiveNewState(PCControllerState state)
    {
        switch (state)
        {
            case PCControllerState.Selectable:
                SetActivePCInstructions();
                break;
            case PCControllerState.PlayingGame:
            case PCControllerState.InActivate:
            default:
                break;
        }

        _pcState.ControllerState = state;

        _pcActivationController.UpdateState(_pcState);

        //May move this to its own class, just because i dont like the using reference.
        //if (_pcState.GetControllerState() == PCControllerState.Activate)
        //    AnalyticsEvent.Custom("PCActivation", new Dictionary<string, object>
        //    {
        //        { "ScanSuccess", name }
        //    });
    }
        
    public void SetActiveGame(string activeGame)
    {
        _pcState.ActiveGame = activeGame;

        PCControllerState state = (string.IsNullOrEmpty(activeGame)) ?
            PCControllerState.NotSelectable :
            PCControllerState.PlayingGame;

        ReceiveNewState(state);
    }

    public PCStateModel GetControllersCurrentStateModel()
    {
        return _pcState;
    }

    public bool IsActivePCVisible()
    {
        return _isVisible;
    }

    public void SetProductionType(ProductionType productionType)
    {
        this.productionType = productionType;
    }

    private void SetActivePCInstructions()
    {
        //This will only work with our 2 pc varieties. modifiying based on active fruit type with two sets of instructions until more varieties are used.
        //FruitType otherFruitType = Messages.AppControllerBus.GetActivePCFruitType() == FruitType.Strawberry ? FruitType.Grape : FruitType.Strawberry;
        //string copy = Instructions.TapToActivate.Replace("{{FRUIT}}", otherFruitType.ToString().Replace("_", " "));

        string copy = (Messages.AppControllerBus.GetActivePCFruitType() != FruitType.Strawberry) ?
            Instructions.TapToActivateStrawberry : Instructions.TapToActivateGrape;
        
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.SetBackgroundCopy, copy);
    }

    IEnumerator AreWeReady()
    {
        yield return new WaitForSecondsRealtime(.001f);

        ReceiveNewState(Messages.AppControllerBus.CanIBeActivePC(this));
    }
}
