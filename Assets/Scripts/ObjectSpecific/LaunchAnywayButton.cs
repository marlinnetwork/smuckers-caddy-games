﻿using UnityEngine;

public class LaunchAnywayButton : MonoBehaviour {

    private string AfterClickInstructions { get; set; }

    public void SetLaunchAnywayProductType(string productType)
    {
        //This code will need to be updated if used again
        //AfterClickInstructions = Instructions.TapToActivate.Replace("{{COPY}}", productType.Replace("_", " "));
    }

    private void OnMouseUp()
    {
        Messages.AppControllerBus.SetActivePCProductionType(ProductionType.Ready);
        Messages.AppControllerBus.SetActivePCState(PCControllerState.Selectable);
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.SetBackgroundCopy, AfterClickInstructions);
    }
}
