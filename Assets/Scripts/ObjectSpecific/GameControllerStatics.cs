﻿using UnityEngine;

public sealed partial class GamesController : MonoBehaviour 
{
    private static GamesController gameController = null;

    private GamesController() { gameController = this; }

    public static GamesController GetCurrentGameController()
    {
        return gameController;
    }
}
