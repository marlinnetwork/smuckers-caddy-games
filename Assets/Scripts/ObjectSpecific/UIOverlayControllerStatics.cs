﻿using UnityEngine;
using Assets.Scripts.Menus.Overlays;

public sealed partial class UIOverlayController : MonoBehaviour
{
    private static UIOverlayController uiOverlayController = null;

    private UIOverlayController() { uiOverlayController = this; }

    public static UIOverlayController GetCurrentUIOverlayController()
    {
        return uiOverlayController;
    }

    public static void SetHasBeenClicked(bool state)
    {
        GetCurrentUIOverlayController()._hasBeenClicked = state;
    }

    public static bool ValidateOverlayCanShowInstructions()
    {
        return ValidateCanPerformActions();
    }

    public static bool ValidateOverlayCanAcceptTaps(bool setHasBeenClicked = false)
    {
        bool state = ValidateCanPerformActions();

        if (GetCurrentUIOverlayController()._hasBeenClicked)
            state = false;

        if (state && setHasBeenClicked)
            SetHasBeenClicked(true);

        return state;
    }

    private static bool ValidateCanPerformActions()
    {
        bool state = true;

        foreach (IContentOverlay overlay in UIOverlayController.GetCurrentUIOverlayController().GetComponentsInChildren<IContentOverlay>())
        {
            if (!overlay.CanPerformActions())
                state = false;
        }

        return state;
    }
}