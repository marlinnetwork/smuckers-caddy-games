﻿using UnityEngine;
using System.Collections.Generic;

//There is no reason to muddle up the app class with these statics.
public sealed partial class AppController : MonoBehaviour
{
    //Singleton esque for the message bus concept.
    private static AppController appController = null;

    //only valid for the appcontroller handle for the message buss.
    private AppController() { appController = this; }

    public static AppController GetCurrentAppController()
    {
        return appController;
    }

    public static PCController GetCurrentActivePC()
    {
        return GetCurrentAppController().ActivePC;
    }

    public static List<PCController> GetCurrentActivePCQueue()
    {
        return GetCurrentAppController().ActivePCQueue;
    }
}