﻿using System.Collections.Generic;
using UnityEngine;

/*
 Statics Availble in ObjectSpecifices
 This has a singleton implimentation added,
 but is currently a pseudo-single by all intents and purposes...
*/

public sealed partial class AppController : MonoBehaviour
{
    // Queue is the current list of active pcs
    private List<PCController> ActivePCQueue = new List<PCController>(); 
    private PCController ActivePC;
    private ResourceService _resourceService;

    public void Awake()
    {
        _resourceService = GetComponentInChildren<ResourceService>();
    }

    private void Start()
    {
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.SetBackgroundCopy, Instructions.MovePCIntoView);
    }

    public void Update()
    {
        IsEscape();
    }

    public GameObject GetFruitResourceByType(FruitType fruitType)
    {
        return _resourceService.GetFruitByFruitType(fruitType);
    }

    public GameObject GetEffectResourceByName(string name)
    {
        return _resourceService.GetEffectByName(name);
    }

    public GameObject GetOverlayResourceByName(string name)
    {
        return _resourceService.GetOverlayByName(name);
    }

    public IEnumerable<Assets.Scripts.Models.FunFactModel> GetFunFacts()
    {
        return _resourceService.GetFunFactModels();
    }

    public IEnumerable<Assets.Scripts.Models.ConversationStarterModel> GetConversationStarters()
    {
        return _resourceService.GetConversationStarterModels();
    }

    public void EnqueueActivePCQueue(PCController controller)
    {
        if (!ActivePCQueue.Contains(controller))
            ActivePCQueue.Add(controller);
    }

    public void DequeueActivePCQueue(PCController controller)
    {
        controller.ReceiveNewState(PCControllerState.InActivate);
        ActivePCQueue.Remove(controller);

        if (ActivePC == controller)
            ActivePC = null;
        
        ActivePCQueue.ForEach(FE => {
            FE.ReceiveNewState(CanIBeActivePC(FE));
        });

        if (ActivePCQueue.Count == 0)
        {
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.SetBackgroundCopy, Instructions.MovePCIntoView);

            ActivePC = null;
        }
    }

    public void SetActivePCProductionType(ProductionType productionType)
    {
        if (ActivePC != null)
            ActivePC.SetProductionType(productionType);
    }

    public void SetActivePCState(PCControllerState state)
    {
        if (ActivePC != null)
        {
            if (ActivePC.IsActivePCVisible())
                ActivePC.ReceiveNewState(state);
            else
                DequeueActivePCQueue(ActivePC);
        }
    }

    public void SetActivePCActiveGame(string activeGame)
    {
        if(ActivePC != null)
            ActivePC.SetActiveGame(activeGame);
    }

    public PCControllerState CanIBeActivePC(PCController controller)
    {
        PCControllerState state = PCControllerState.InActivate;
        
        //Do we have a current active pc
        if (ActivePC == null)
        {
            // Only 1 in current queue
            if (ActivePCQueue.Count == 1)
            {
                ActivePC = controller;

                state = PCControllerState.Selectable;
            }
        }
        else
        {
            state = (ActivePC == controller) ?
                    controller.GetControllersCurrentStateModel().ControllerState :
                    PCControllerState.InActivate;
        }

        return state;
    }

    private void IsEscape()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
                activity.Call<bool>("moveTaskToBack", true);
            }
            else
            {
                Application.Quit();
            }

            StopAllCoroutines();
        }
    }
}
