﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Models;

//A tad heavy, but the cleanest way I could thing to separate messageing if we need
// without events. I am concerned about overflow or mis registration and un registration
public static partial class Messages
{
    public static class AppControllerBus
    {
        //Get
        public static GameObject GetFruitResourceByType(FruitType fruitType)
        {
            return AppController.GetCurrentAppController().GetFruitResourceByType(fruitType);
        }

        public static GameObject GetEffectResourceByName(string name)
        {
            return AppController.GetCurrentAppController().GetEffectResourceByName(name);
        }

        public static GameObject GetOverlayResourceByName(string name)
        {
            return AppController.GetCurrentAppController().GetOverlayResourceByName(name);
        }

        public static IEnumerable<FunFactModel> GetFunFacts()
        {
            return AppController.GetCurrentAppController().GetFunFacts();
        }

        public static IEnumerable<ConversationStarterModel> GetConversationStarters()
        {
            return AppController.GetCurrentAppController().GetConversationStarters();
        }

        public static PCControllerState CanIBeActivePC(PCController controller)
        {
            return AppController.GetCurrentAppController().CanIBeActivePC(controller);
        }

        public static FruitType GetActivePCFruitType()
        {
            return AppController.GetCurrentActivePC().fruitType;
        }

        //Set
        public static void SetActivePCProductionType(ProductionType productionType)
        {
            AppController.GetCurrentAppController().SetActivePCProductionType(productionType);
        }

        public static void SetActivePCState(PCControllerState state)
        {
            AppController.GetCurrentAppController().SetActivePCState(state);
        }

        public static void EnqueueActivePCQueue(PCController controller)
        {
            AppController.GetCurrentAppController().EnqueueActivePCQueue(controller);
        }

        public static void DequeueActivePCQueue(PCController controller)
        {
            AppController.GetCurrentAppController().DequeueActivePCQueue(controller);
        }

        public static void SetActivePCActiveGame(string activeGame)
        {
            AppController.GetCurrentAppController().SetActivePCActiveGame(activeGame);
        }
    }
}