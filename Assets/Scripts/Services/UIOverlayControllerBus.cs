﻿using UnityEngine;

public static partial class Messages
{
    public static class UIOverlayControllerBus
    {
        public static void SendMessage(UIMessageFor messageFor, object messageType, object payload = null)
        {
            UIOverlayController.GetCurrentUIOverlayController().Message(messageFor, messageType, payload);
        }

        public static string GetUIOverlayCurrentText()
        {
            return UIOverlayController.GetCurrentUIOverlayController().GetInstructionTextValue;
        }
        
        public static GameObject GetOverlayResourceByName(string name)
        {
            return AppControllerBus.GetOverlayResourceByName(name);
        }

        public static void SetHasBeenClicked(bool state)
        {
            UIOverlayController.SetHasBeenClicked(state);
        }
    }
}