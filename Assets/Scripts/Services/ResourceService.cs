﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Assets.Scripts.Models;

public class ResourceService : MonoBehaviour {
    private const string jsonFolder = "Json";

    private IList<GameObject> AvailableFruit;
    private IList<GameObject> AvailableEfect;
    private IList<GameObject> AvailableOverlays;
    private FunFacts AvailableFunFacts;
    private ConversationStarters AvailableConversationStarters;

    private void Awake()
    {
        AvailableFruit = LoadAll("Fruit");
        AvailableEfect = LoadAll("Effect");
        AvailableOverlays = LoadAll("Overlays");

        BetterStreamingAssets.Initialize();

        AvailableFunFacts = LoadJSON<FunFacts>("FunFacts.json");
        AvailableConversationStarters = LoadJSON<ConversationStarters>("ConversationStarters.json");
    }

    private IList<GameObject> LoadAll(string name)
    {
        IList<GameObject> list = null;

        UnityEngine.Object[] i = Resources.LoadAll(name, typeof(GameObject));

        if (i != null)
        {
            list = new List<GameObject>();

            foreach (UnityEngine.Object obj in i)
            {
                list.Add((GameObject)obj);
            }
        }

        return list;
    }

    public GameObject GetFruitByFruitType(FruitType fruitType)
    {
        return AvailableFruit.FirstOrDefault(FOD => FOD.transform.name.ToLower() == fruitType.ToString().ToLower());
    }

    public GameObject GetEffectByName(string name)
    {
        return AvailableEfect.FirstOrDefault(FOD => FOD.transform.name.ToLower() == name.ToLower());
    }

    public GameObject GetOverlayByName(string name)
    {
        return AvailableOverlays.FirstOrDefault(FOD => FOD.transform.name.ToLower() == name.ToLower());
    }
    
    public IEnumerable<FunFactModel> GetFunFactModels()
    {
        return AvailableFunFacts.results;
    }

    public IEnumerable<ConversationStarterModel> GetConversationStarterModels()
    {
        return AvailableConversationStarters.results;
    }

    private T LoadJSON<T>(string filename)
    {
        //may want to make dif calls between android and iOS
        //if (Application.platform == RuntimePlatform.Android)
        
        string filePath = Path.Combine(jsonFolder, filename);

        if (BetterStreamingAssets.FileExists(filePath))
        {
            try {
                string dataAsJson = BetterStreamingAssets.ReadAllText(filePath);

                return JsonUtility.FromJson<T>("{\"results\":" + dataAsJson + "}");
            }
            catch
            {
                Debug.LogError("Cannot load game data!");

                return default(T);
            }
        }
        else
        {
            Debug.LogError("Cannot load game data!");

            return default(T);
        }
    }
}
