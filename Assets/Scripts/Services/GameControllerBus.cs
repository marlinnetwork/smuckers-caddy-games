﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Models;

public static partial class Messages
{
    public static class GamesControllerBus
    {
        public static string GetActiveGame()
        {
            return GamesController.GetCurrentGameController().GetActiveGame();
        }

        public static void SetCurrentActiveGame(string game)
        {
            GamesController.GetCurrentGameController().SetActiveGame(game);

            AppControllerBus.SetActivePCActiveGame(game);
        }

        //Game Specifics
        public static IList<FunFactModel> GetFunFacts()
        {
            return AppControllerBus.GetFunFacts().ToList();
        }

        public static IList<ConversationStarterModel> GetConverationStarters()
        {
            return AppControllerBus.GetConversationStarters().ToList();
        }

        public static string GetNextAvailableCategoryForActiveGame()
        {
            return GamesController.GetCurrentGameController().GetNextAvailableCategory();
        }

        public static FunFactModel GetNextFunFactByCategoryName(string category)
        {
            return (FunFactModel)GamesController.GetCurrentGameController().GetNextCategory(category);
        }

        public static ConversationStarterModel GetNextConversationStarter()
        {
            return (ConversationStarterModel)GamesController.GetCurrentGameController().GetNextCategory();
        }

        //Settings
        public static BaseContentOverlay GetActiveGameSettings()
        {
            return GamesController.GetCurrentGameController().GetActiveGameSettings();
        }

        public static void SetActiveGameSettings(BaseContentOverlay settings)
        {
            GamesController.GetCurrentGameController().SetActiveGameSettings(settings);
        }

        public static string GetActiveGameInstructions()
        {
            return GamesController.GetCurrentGameController().GetActiveGameInstructions();
        }

        //Status
        public static BaseContentOverlay GetActiveGameState()
        {
            return GamesController.GetCurrentGameController().GetActiveGameState();
        }

        public static void ResetActiveGameState()
        {
            GamesController.GetCurrentGameController().ResetActiveGameState();
        }

        //State
        public static void UpdateActiveGameTime(int time)
        {
            GamesController.GetCurrentGameController().UpdateActiveGameTime(time);
        }

        public static void SetActiveGameMode(GameMode mode)
        {
            GamesController.GetCurrentGameController().SetActiveGameMode(mode);
        }

        public static void UpdateActiveGameScore(int score)
        {
            GamesController.GetCurrentGameController().UpdateActiveGameScore(score);
        }
    }
}