﻿using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine;

public class CameraFocusController : MonoBehaviour {

	void Start () {
		
		var vuforia = VuforiaARController.Instance;
		vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
		vuforia.RegisterVuforiaInitializedCallback(OnVuforiaInitialized);
		vuforia.RegisterOnPauseCallback(OnPaused);

	}

	private void OnVuforiaInitialized() {

		#if UNITY_IOS
			if(UnityEngine.iOS.Device.generation.ToString().Contains("iPad"))
			{
				Debug.Log("Look's like we're running on iPad, so we'll set CameraDeviceMode to optimize speed.");
				CameraDevice.Instance.SelectVideoMode(CameraDevice.CameraDeviceMode.MODE_OPTIMIZE_SPEED);
			}
		#endif
	}

	private void OnVuforiaStarted()
	{
		CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
	}
	private void OnPaused(bool isPaused)
	{
		if (!isPaused)
		{
			CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		}
	}
	
}
