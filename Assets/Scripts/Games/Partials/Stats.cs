﻿using UnityEngine;
using Assets.Scripts.Games;
using System.Collections.Generic;

public sealed partial class GamesController : MonoBehaviour
{
    public void ResetActiveGameStats()
    {
        if (GetCurrentActiveGame() is IStats game)
               game.ResetStats();
    }

    public Dictionary<string, object> GetActiveGameEndingStats()
    {
        return (GetCurrentActiveGame() is IStats game) ?
               game.GetEndingStats() :
               null;
    }

    public int GetActiveGameTimePlayed()
    {
        return (GetCurrentActiveGame() is IStats game) ?
               game.GetTimePlayed() :
               0;
    }
}
