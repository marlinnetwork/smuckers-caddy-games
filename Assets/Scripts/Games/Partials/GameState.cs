﻿
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Games;
using Assets.Scripts.Models;

public sealed partial class GamesController : MonoBehaviour
{
    public BaseContentOverlay GetActiveGameState()
    {
        return (GetCurrentActiveGame() is IState game) ?
            game.GetState() :
            null;
    }

    public void ResetActiveGameState()
    {
        if (GetCurrentActiveGame() is IState game)
            game.ResetState();
    }

    public void UpdateActiveGameTime(int time)
    {
        if (GetCurrentActiveGame() is IState game)
            game.UpdateTime(time);
    }

    public void SetActiveGameMode(GameMode mode)
    {
        if (GetCurrentActiveGame() is IState game)
            game.SetGameMode(mode);
    }

    public void UpdateActiveGameScore(int score)
    {
        if (GetCurrentActiveGame() is IState game)
            game.UpdateScore(score);
    }
}