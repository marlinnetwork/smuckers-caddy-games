﻿using UnityEngine;
using Assets.Scripts.Games;
using Assets.Scripts.Models;

public sealed partial class GamesController : MonoBehaviour
{
    public BaseContentOverlay GetActiveGameSettings()
    {
        //PATTERN MATCHING dotnet 7
        return (GetCurrentActiveGame() is ISettings game) ?
            game.GetSettings() :
            null;

        //This may not be do able
        //If we have issue with the version 7
        //ISettings game = GetActiveGame() as ISettings;

        //return (game != null) ?
        //    game.GetSettings() :
        //    null;
    }

    public void SetActiveGameSettings(BaseContentOverlay settings)
    {
        if (GetCurrentActiveGame() is ISettings game)
            game.SetSettings(settings);

        //If we have issue with the version 7
        //ISettings game = GetActiveGame() as ISettings;

        //if (game != null)
        //    game.SetSettings(settings);
    }
}
