﻿using UnityEngine;
using Assets.Scripts.Games;
using Assets.Scripts.Models;

public sealed partial class GamesController : MonoBehaviour
{
    //Using a combination of interfacts and inheritance
    public BaseDataModel GetNextCategory(string category)
    {
        return ((BaseCategoryGame)GetCurrentActiveGame()).GetNext(category);
    }

    public BaseDataModel GetNextCategory()
    {
        return ((BaseCategoryGame)GetCurrentActiveGame()).GetNext();
    }

    public string GetNextAvailableCategory()
    {
        return ((BaseCategoryGame)GetCurrentActiveGame()).GetNextRandomAvailableCategory();
    }
}
