﻿using System.Collections.Generic;
using Assets.Scripts.Models;
using System.Linq;

namespace Assets.Scripts.Games
{
    public abstract class BaseCategoryGame : BaseGame
    {
        public abstract Models.BaseDataModel GetNext();

        public abstract Models.BaseDataModel GetNext(string Category);

        public abstract string GetNextRandomAvailableCategory();

        public virtual List<KeyValuePair<string, int>> AggregateAndSum(string[] used)
        {
            return used
                .GroupBy(GB => GB.ToString())
                .Select(S => new KeyValuePair<string, int>(S.Key, S.Count()))
                .ToList();
        }

        protected abstract string[] GetAllUsedKeys();

        protected abstract void RefreshAllCategories();

        protected abstract void RemoveDisabledCategories();

        protected abstract void RefereshCategory(string category);

        protected abstract void ValidateAvailable();

        protected abstract void ValidateAvailable(string category);

        protected abstract void UpdateFromAvailableToUsed(BaseDataModel model);

        protected abstract void UpdateFromUsedToAvailable(BaseDataModel model);
    }
}