﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

using Assets.Scripts.Models;

namespace Assets.Scripts.Games
{
    public class FruitSmashController : BaseGame, ISettings, IState
    {
        private int currentTime;
        private int startTime;
        private GameMode mode;

        private void Awake()
        {
            currentTime = 30;
            startTime = 30;
            score = 0;
            mode = GameMode.Created;
        }

        public override string GetInstructions()
        {
            return Instructions.FruitSmash;
        }

        //ISettings
        public BaseContentOverlay GetSettings()
        {
            return new FruitSmashSettingsModel()
            {
                Time = startTime,
                Score = score
            };
        }

        public void SetSettings(BaseContentOverlay Settings)
        {
            FruitSmashSettingsModel model = (FruitSmashSettingsModel)Settings;

            score = model.Score;
            currentTime = model.Time;
            startTime = model.Time;
        }

        //IGameState
        public BaseContentOverlay GetState()
        {
            return new FruitSmashGameModel()
            {
                Time = currentTime,
                StartTime = startTime,
                GameMode = mode,
                Score = score
            };
        }

        //IStats
        public override void ResetStats()
        {
            base.ResetStats();
        }

        public override Dictionary<string, object> GetEndingStats()
        {
            return new Dictionary<string, object>()
            {
                {"fruit_popped", Score},
                {GameTimeSpend, GetTimePlayed()}
            };
        }

        public void UpdateTime(int time)
        {
            this.currentTime += time;
        }

        public void SetGameMode(GameMode mode)
        {
            this.mode = mode;
        }
    }
}
