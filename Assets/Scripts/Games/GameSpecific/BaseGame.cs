﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Games
{
    public abstract class BaseGame : MonoBehaviour, IGame, IStats
    {
        // Some Analytics Constants? This will move as will the analytics service
        // as we get more games.. but for timing the abstraction is not.
        protected const string GameTimeSpend = "game_time_spent";

        public string Name { get { return name; } }

        public int Score { get {return score; }}

        protected DateTime PlayTime;

        public BaseGame GetCurrentInstance()
        {
            return this;
        }

        protected int score;

        public void UpdateScore(int score)
        {
            this.score += score;
        }

        public virtual void ResetState()
        {
            score = 0;
            PlayTime = DateTime.Now;
        }

        public abstract string GetInstructions();

        public virtual void ResetStats()
        {
            PlayTime = new DateTime(0);
        }

        public abstract Dictionary<string, object> GetEndingStats();

        public int GetTimePlayed()
        {
            return (int)DateTime.Now.Subtract(PlayTime).TotalSeconds;
        }
    }
}
