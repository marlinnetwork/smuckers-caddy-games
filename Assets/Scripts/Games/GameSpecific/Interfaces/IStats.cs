﻿using System.Collections.Generic;

namespace Assets.Scripts.Games
{ 
    public interface IStats
    {
        //string GetStats();

        void ResetStats();

        Dictionary<string, object> GetEndingStats();

        int GetTimePlayed();
    }
}
