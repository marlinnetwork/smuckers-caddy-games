﻿using Assets.Scripts.Models;

namespace Assets.Scripts.Games
{
    public interface IGame
    {
        string Name { get; }

        int Score { get; }

        BaseGame GetCurrentInstance();
    }
}