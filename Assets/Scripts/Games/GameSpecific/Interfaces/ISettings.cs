﻿namespace Assets.Scripts.Games
{
    public interface ISettings
    {
        Models.BaseContentOverlay GetSettings();

        void SetSettings(Models.BaseContentOverlay Settings);
    }
}
