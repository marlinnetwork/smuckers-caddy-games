﻿namespace Assets.Scripts.Games
{
    public interface IState
    {
        Models.BaseContentOverlay GetState();

        void ResetState();

        void UpdateTime(int time);

        void SetGameMode(GameMode mode);

        void UpdateScore(int score);
    }
}
