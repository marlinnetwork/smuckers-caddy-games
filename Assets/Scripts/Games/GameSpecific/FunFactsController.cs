﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

using Assets.Scripts.Models;
using Assets.Scripts.Helpers;

namespace Assets.Scripts.Games
{
    public class FunFactsController : BaseCategoryGame, ISettings
    {
        private IList<string> disabledFunFacts;
        private IList<FunFactModel> used;
        private IList<FunFactModel> available;

        private void Awake()
        {
            disabledFunFacts = new List<string>();
            used = new List<FunFactModel>();
        }

        private void Start()
        {
            available = Messages.GamesControllerBus.GetFunFacts().Shuffle();
        }

        public override string GetInstructions()
        {
            return Instructions.Funfacts;
        }

        //ISettings
        public BaseContentOverlay GetSettings()
        {
            return new FunFactsSettingsModel()
            {
                DisabledFunFacts = disabledFunFacts.ToArray()
            };
        }

        public void SetSettings(BaseContentOverlay Settings)
        {
            disabledFunFacts = ((FunFactsSettingsModel)Settings).DisabledFunFacts.ToList();

            RefreshAllCategories();
            RemoveDisabledCategories();
        }

        //IStats
        public override void ResetStats()
        {
            base.ResetStats();

            //Update whats been counted.
            used.ToList().ForEach(FE => FE.Counted = true);
        }

        public override Dictionary<string, object> GetEndingStats()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                {GameTimeSpend, GetTimePlayed()}
            };

            AggregateAndSum(GetAllUsedKeys()).ForEach(FE => dic.Add(FE.Key, FE.Value));

            return dic;
        }

        //Public BaseCategory
        public override BaseDataModel GetNext()
        {
            ValidateAvailable();

            FunFactModel model = available.FirstOrDefault();

            UpdateFromAvailableToUsed(model);

            return model;
        }

        public override BaseDataModel GetNext(string category)
        {
            ValidateAvailable();

            FunFactModel model = available.FirstOrDefault(FOD => FOD.Category == category);

            UpdateFromAvailableToUsed(model);

            return model;
        }

        public override string GetNextRandomAvailableCategory()
        {
            ValidateAvailable();
           
            return available.Shuffle().FirstOrDefault()?.Category;
        }

        //protected BaseCateogry
        protected override void RefreshAllCategories()
        {
            used.ToList().ForEach(FE =>
            {
                UpdateFromUsedToAvailable(FE);
            });
        }

        protected override void RemoveDisabledCategories()
        {
            available.ToList().ForEach(FE =>
            {
                if(disabledFunFacts.Any(a => a.ToLower() == FE.Category.ToLower()))
                    UpdateFromAvailableToUsed(FE);
            });
        }

        protected override void RefereshCategory(string category)
        {
            used.Where(W => W.Category == category).ToList().ForEach(FE =>
            {
                UpdateFromUsedToAvailable(FE);
            });
        }
        
        protected override void ValidateAvailable(string category)
        {
            if (!available.Any(A => A.Category == category))
                RefereshCategory(category);
        }

        protected override void ValidateAvailable()
        {
            if (available.Count() < 1)
                RefreshAllCategories();
        }

        protected override void UpdateFromAvailableToUsed(BaseDataModel baseModel)
        {
            FunFactModel model = (FunFactModel)baseModel;

            used.Add(model);
            available.Remove(model);
        }

        protected override void UpdateFromUsedToAvailable(BaseDataModel baseModel)
        {
            FunFactModel model = (FunFactModel)baseModel;
            available.Add(model);
            used.Remove(model);
        }

        protected override string[] GetAllUsedKeys()
        {
            return used.Where(W => !W.Counted).Select(S => S.Category).ToArray();
        }
    }
}