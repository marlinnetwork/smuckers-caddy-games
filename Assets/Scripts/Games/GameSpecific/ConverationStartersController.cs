﻿using System.Collections.Generic;
using Assets.Scripts.Models;
using System.Linq;
using Assets.Scripts.Helpers;

namespace Assets.Scripts.Games
{
    public class ConverationStartersController : BaseCategoryGame
    {
        private IList<ConversationStarterModel> used;

        private IList<ConversationStarterModel> available;

        private void Awake()
        {
            used = new List<ConversationStarterModel>();
        }

        private void Start()
        {
            available = Messages.GamesControllerBus.GetConverationStarters().Shuffle();
        }

        public override string GetInstructions()
        {
            return Instructions.ConversationStarters;
        }
        
        //IStats
        public override void ResetStats()
        {
            base.ResetStats();

            used.ToList().ForEach(FE => FE.Counted = true);
        }

        public override Dictionary<string, object> GetEndingStats()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>()
            {
                {GameTimeSpend, GetTimePlayed()}
            };

            AggregateAndSum(GetAllUsedKeys()).ForEach(FE => dic.Add(FE.Key, FE.Value));
            
            return dic;
        }

        //Public BaseCateogry
        public override BaseDataModel GetNext()
        {
            ValidateAvailable();

            ConversationStarterModel model = available.FirstOrDefault();

            UpdateFromAvailableToUsed(model);

            return model;
        }

        public override BaseDataModel GetNext(string category)
        {
            ValidateAvailable(category);

            ConversationStarterModel model = available.FirstOrDefault(FOD => FOD.Category == category);

            UpdateFromAvailableToUsed(model);

            return model;
        }

        public override string GetNextRandomAvailableCategory()
        {
            ValidateAvailable();

            return available.Shuffle().FirstOrDefault().Category;
        }

        //protected BaseCateogry
        protected override void RefreshAllCategories()
        {
            used.ToList().ForEach(FE =>
            {
                UpdateFromUsedToAvailable(FE);
            });
        }

        protected override void RemoveDisabledCategories()
        {
            available.ToList().ForEach(FE =>
            {
                //if (disabledFunFacts.Contains(FE.Category))
                //    UpdateFromAvailableToUsed(FE);
            });
        }

        protected override void RefereshCategory(string category)
        {
            used.Where(W => W.Category == category).ToList().ForEach(FE =>
            {
                UpdateFromUsedToAvailable(FE);
            });
        }

        protected override void ValidateAvailable(string category)
        {
            if (!available.Any(A => A.Category == category))
                RefereshCategory(category);
        }

        protected override void ValidateAvailable()
        {
            if (available.Count() < 1)
                RefreshAllCategories();
        }

        protected override void UpdateFromAvailableToUsed(BaseDataModel baseModel)
        {
            ConversationStarterModel model = (ConversationStarterModel)baseModel;

            used.Add(model);
            available.Remove(model);
        }

        protected override void UpdateFromUsedToAvailable(BaseDataModel baseModel)
        {
            ConversationStarterModel model = (ConversationStarterModel)baseModel;

            available.Add(model);
            used.Remove(model);
        }

        protected override string[] GetAllUsedKeys()
        {
            return used.Where(W => !W.Counted).Select(S => S.Category).ToArray();
        }
    }
}