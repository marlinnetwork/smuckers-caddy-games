﻿
using UnityEngine;
using UnityEngine.Analytics;
using Assets.Scripts.Games;
using System.Collections.Generic;

public sealed partial class GamesController : MonoBehaviour
{
    private IDictionary<string, IGame> gamesDictionary;

    private string activeGame { get; set; }

    //private DateTime lastGameStartTime;

    private Dictionary<string, int> sessionPlays;
    private Dictionary<string, int> sessionTimes;
    private Dictionary<string, int> levelExits;

    private void Awake()
    {
        gamesDictionary = new Dictionary<string, IGame>();
        sessionPlays = new Dictionary<string, int>();
        sessionTimes = new Dictionary<string, int>();
        levelExits = new Dictionary<string, int>();

        //This should be a factory...
        gamesDictionary.Add(LoadDictionaryItem(GetComponentInChildren<ConverationStartersController>()));
        gamesDictionary.Add(LoadDictionaryItem(GetComponentInChildren<FruitSmashController>()));
        gamesDictionary.Add(LoadDictionaryItem(GetComponentInChildren<FunFactsController>()));

        foreach (var pair in gamesDictionary)
        {
            sessionPlays.Add(pair.Key, 0);
            sessionTimes.Add(pair.Key, 0);
            levelExits.Add(pair.Key, 0);
        }
    }

    public string GetActiveGame() { return activeGame; }

    public void SetActiveGame(string nextActiveGame)
    {
        // Lets update the count of game played this session
        if (!string.IsNullOrEmpty(nextActiveGame))
            UpdateAnalyticsDictionary(sessionPlays, nextActiveGame, 1);

        // if activeGame exists we are either ending or changes a game
        if (!string.IsNullOrEmpty(activeGame))
        {
            UpdateAnalyticsDictionary(levelExits, activeGame, 1);
            UpdateAnalyticsDictionary(sessionTimes, activeGame, GetActiveGameTimePlayed());

            foreach (var item in GetActiveGameEndingStats())
            {
                Debug.Log("eventName: EndGameStats key: " + item.Key + " value: " + item.Value.ToString());
            }

            //Agnostic ending game starts
            AnalyticsEvent.GameOver(activeGame, GetActiveGameEndingStats());
        }

        activeGame = nextActiveGame;

        ResetActiveGameStats();
    }

    private void OnDestroy()
    {
        SendAnalytics("session_plays", sessionPlays);
        SendAnalytics("session_times", sessionTimes);
        SendAnalytics("level_quits", levelExits);
    }

    private void SendAnalytics(string eventName, Dictionary<string, int> referenceDict)
    {
        var analytics = new Dictionary<string, object>();

        foreach(var pair in referenceDict)
        {
            analytics.Add(pair.Key, pair.Value);
            Debug.Log("eventName: " + eventName + " key: " + pair.Key + " value: " + pair.Value);
        }
        
        Analytics.CustomEvent(eventName, analytics);
    }

    private void UpdateAnalyticsDictionary(Dictionary<string, int> dict, string game, int incrementor)
    {
        if(dict.ContainsKey(game))
            dict[game] += incrementor;
        else
            dict.Add(game, incrementor);
    }

    public string GetActiveGameInstructions()
    {
        return (GetCurrentActiveGame() is BaseGame game) ?
            game.GetInstructions() :
            string.Empty;
    }

    private IGame GetCurrentActiveGame()
    {
        IGame game = null;

        gamesDictionary.TryGetValue(activeGame, out game);

        return game;
    }

    private KeyValuePair<string, IGame> LoadDictionaryItem(IGame obj)
    {
        return new KeyValuePair<string, IGame>(obj.Name.ToLower(), obj);
    }
}
