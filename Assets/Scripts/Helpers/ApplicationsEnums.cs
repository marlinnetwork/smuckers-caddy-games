﻿//Inactive: nothing happening.
//Selectable: lid and fruit are/have spawned.
//NotSelectable: lid has been tapped, but no game yet.
//PlayingGame: PC in view Game activly being played.
//NotVisiblePlayingGame: pc has been lost but a game is running.
public enum PCControllerState { InActivate, Selectable, NotSelectable, PlayingGame, NotVisiblePlayingGame }

public enum ProductionType { Ready/*, LaunchAnyway */}

public enum CopyAndBackgroundType { Cover, SpeechBubble, LaunchAnyway }

public enum FruitType { Strawberry, Grape }

public enum ProductType { Fruit_Butter, Conserves, Jam, Jelly, Marmalade, Preserves, Spread }

public enum UIMessageFor { Instructions, Menus, Games }

public enum UIInstructions { SetBackgroundCopy, ResetCurrentTimer, ShowInstructions, HideInstructions }

public enum GameMenuOverlay { Load, RemoveCurrent }

public enum RelativeLocation {Center, Top, Right, Bottom, Left, TopRight, BottomRight, BottomLeft, TopLeft }

public enum GameMode { Created, Playing, Paused, Completed }