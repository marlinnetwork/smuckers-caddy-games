﻿using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    // The update method can be an efficiency drag becaused of the constant polling. Impliment via properties and coroutines...
    //Interface on these guys would make it easier ish to use.
    private Vector3 StartPosition;
    private Vector3 NextPosition;
    private IList<Vector3> CurvePositions;
    private float CurrentMoveTime;

    public float MoveTime = 1f;

    private void Awake()
    {
        CurrentMoveTime = 0f;
        SetToCurrentTransform();

        CurvePositions = new List<Vector3>();
    }

    private void Update()
    {
        if (StartPosition != NextPosition)
        {
            CurrentMoveTime += Time.deltaTime;

            if (CurrentMoveTime > MoveTime)
                CurrentMoveTime = MoveTime;

            transform.localPosition = (CurvePositions != null && CurvePositions.Count > 0) ?
                GetCurvePoint(StartPosition, CurvePositions[0], CurvePositions[1], NextPosition, Easing.SmootherStep(CurrentMoveTime, MoveTime)) :
                Vector3.Lerp(StartPosition, NextPosition, Easing.EaseIn(CurrentMoveTime, MoveTime));

            if (NextPosition == transform.localPosition)
            {
                StartPosition = NextPosition;
                CurvePositions = null;
            }
        }
    }

    public void SetToPosition(Vector3 pos)
    {
        transform.localPosition = pos;

        SetToCurrentTransform();
    }

    public void SetNextPosition(Vector3 trans, Vector3 curvePosition1, Vector3 curvePosition2, float time = 1f)
    {
        CurrentMoveTime = 0f;
        NextPosition = trans;

        MoveTime = time;

        if(curvePosition1 != Vector3.zero && curvePosition2 != Vector3.zero)
        {
            CurvePositions = new List<Vector3>()
            {
                curvePosition1, curvePosition2
            };
        }
    }

    private void SetToCurrentTransform()
    {
        StartPosition = transform.localPosition;
        NextPosition = transform.localPosition;
    }

    private Vector3 GetCurvePoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
	{
		t = Mathf.Clamp01 (t);

		float oneMinusT = 1f - t;

		return oneMinusT * oneMinusT * oneMinusT * p0 +
				3f * oneMinusT * oneMinusT * t * p1 +
				3f * oneMinusT * t * p2 +
				t * t * t * p3;
	}
}

