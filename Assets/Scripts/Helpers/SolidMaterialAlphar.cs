﻿using UnityEngine;

public class SolidMaterialAlphar : MonoBehaviour {

    // The update method can be an efficiency drag becaused of the constant polling. Impliment via properties and coroutines...
    private Renderer rend;
    private float StartAlpha { get; set; }
    private float NextAlpha { get; set; }
    private float BeginAlpha { get; set; }
    private float CurrentAlphaTime { get; set; }
    public float alphaTime { get; set; }

    private bool PingPong { get; set; }

    private void Awake ()
    {
        rend = GetComponent<Renderer>();

        CurrentAlphaTime = 0f;
        alphaTime = 1f;

        SetToCurrentAlpha();
	}
	
	private void Update ()
    {
		if(StartAlpha != NextAlpha)
        {
            CurrentAlphaTime += Time.deltaTime;

            if (CurrentAlphaTime > alphaTime)
                CurrentAlphaTime = alphaTime;

            Color color = rend.material.color;

            color.a = Mathf.Lerp(StartAlpha, NextAlpha, Easing.EaseIn(CurrentAlphaTime, alphaTime));

            rend.material.color = color;

            if (NextAlpha == rend.material.color.a)
            {
                StartAlpha = NextAlpha;

                if(PingPong)
                {
                    SetNextAlpha(BeginAlpha, alphaTime);
                }
            }
        }
	}

    public void SetToAlpha(float alpha)
    {
        Color color = rend.material.color;

        color.a = alpha;

        rend.material.color = color;

        SetToCurrentAlpha();
    }

    public void SetNextAlpha (float alpha, float time = 1f, bool pingPong = false)
    {
        CurrentAlphaTime = 0;
        NextAlpha = alpha;
        alphaTime = time;
        PingPong = pingPong;
    }

    private void SetToCurrentAlpha()
    {
        StartAlpha = rend.material.color.a;
        NextAlpha = rend.material.color.a;
        BeginAlpha = rend.material.color.a;
    }
}
