﻿//This houses the available states of the instructions ui overlay
// The overlay instance will have the current state and can convert it to copy for display when needed.
public static class Instructions
{
    public static readonly string MovePCIntoView = "Slide a Smucker’s® Strawberry or Grape spread into view.";
    public static readonly string MoveIsPlayingPCBackIntoView = "Slide a Smucker’s® {{COPY}} spread back into view.";

    //public static readonly string MovePCIntoView = "Slide a Smucker's® Strawberry, Grape or Mixed Fruit Spread into view.";
    //public static readonly string TapToActivate = "Tap the lid to choose a game, or switch to a Smucker’s {{FRUIT}} {{TYPE}}.";
    public static readonly string TapToActivateStrawberry = "Tap the lid to choose a game, or switch to a Smucker’s Strawberry spread.";
    public static readonly string TapToActivateGrape = "Tap the lid to choose a game, or switch to a Smucker’s Grape Jelly.";

    //public static readonly string TapToActivate = "Tap your {{COPY}} lid to choose a game.";
    //public static readonly string NotQuiteRipe = "Tap launch anyway to begin the suggested experience.";
    //public static readonly string MultipleNonActivePCS = "Only one (1) type of Jelly can be active at a time.";
    //public static readonly string AciveOnly1 = "Only one (1) pc can be active at a time";

    //Game Instructions
    public static readonly string ConversationStarters = "Tap the fruit to reveal conversation starters.";
    public static readonly string Funfacts = "Tap the fruit to reveal trivia questions. ";
    public static readonly string FruitSmash = "Tap fruit to start smashing!";

    public static readonly string AtLeastOnCategory = "At least one(1) {{COPY}} category needs to be ON.";
    //public static readonly string PostActiveVisibleInactive = "Use the back button to reset select a new pc";
}