﻿using System.Collections.Generic;
using UnityEngine;

public class Scaler : MonoBehaviour
{
    // The update method can be an efficiency drag becaused of the constant polling. Impliment via properties and coroutines...
    private Vector3 StartScale { get; set; }
    private Vector3 NextScale { get; set; }

    private Vector3 BeginScale { get; set; }

    private IList<Vector3> DistortScale;

    private float CurrentScaleTime { get; set; }

    private bool PingPong { get; set; }

    [SerializeField]
    public float ScaleTime;
    
    private void Awake()
    {
        DistortScale = new List<Vector3>();
        CurrentScaleTime = 0f;
        ScaleTime = 1f;

        SetToCurrentTransform();
        
    }

    private void Update()
    {
        if(StartScale != NextScale)
        {
            CurrentScaleTime += Time.deltaTime;

            if (CurrentScaleTime > ScaleTime)
                CurrentScaleTime = ScaleTime;

            transform.localScale = (DistortScale != null && DistortScale.Count > 0) ?
                GetDistortionScale(StartScale, DistortScale[0], DistortScale[1], NextScale, Easing.SmootherStep(CurrentScaleTime, ScaleTime)) :
                Vector3.Lerp(StartScale, NextScale, Easing.EaseIn(CurrentScaleTime, ScaleTime));

            if (NextScale == transform.localScale)
            {
                StartScale = NextScale;

                if (PingPong)
                {
                    if (DistortScale != null && DistortScale.Count > 0)
                        SetNextScale(BeginScale, DistortScale[0], DistortScale[1], ScaleTime);
                    else
                        SetNextScale(BeginScale, Vector3.zero, Vector3.zero, ScaleTime);
                }
                else
                    DistortScale = null;
            }
        }
    }

    public void SetToScale(Vector3 scale)
    {
        transform.localScale = scale;

        SetToCurrentTransform();
    }

    public void SetNextScale(Vector3 trans, Vector3 distortScale1, Vector3 distortScale2, float time = 1f, bool pingPong = false)
    {
        CurrentScaleTime = 0f;
        NextScale = trans;

        ScaleTime = time;

        if (distortScale1 != Vector3.zero && distortScale2 != Vector3.zero)
        {
            DistortScale = new List<Vector3>()
            {
                distortScale1, distortScale2
            };
        }

        PingPong = pingPong;
    }

    public void SetNextScale(float scaleFactor, float time = 1f, bool pingPong = false)
    {
        CurrentScaleTime = 0;
        NextScale = StartScale * scaleFactor;
        ScaleTime = time;
        PingPong = pingPong;
    }

    private void SetToCurrentTransform()
    {
        StartScale = transform.localScale;
        NextScale = transform.localScale;
        BeginScale = transform.localScale;
    }

    private Vector3 GetDistortionScale(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);

        float oneMinusT = 1f - t;

        return oneMinusT * oneMinusT * oneMinusT * p0 +
                3f * oneMinusT * oneMinusT * t * p1 +
                3f * oneMinusT * t * p2 +
                t * t * t * p3;
    }
}