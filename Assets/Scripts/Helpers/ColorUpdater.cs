﻿using UnityEngine;

public class ColorUpdater : MonoBehaviour {

    private Renderer rend;

    private void Awake ()
    {
        rend = GetComponent<Renderer>();
    }

    public void UpdateColor(Color color)
    {
        rend.material.color = color;
    }
}