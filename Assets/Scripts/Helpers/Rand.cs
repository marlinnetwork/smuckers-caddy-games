﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Helpers
{
    public static class Rand
    {
        public static Vector3 GetRandom(float min, float max, bool incudeNegative = true)
        {
            float x = GetRandomFloat(min, max, incudeNegative);
            float y = GetRandomFloat(min, max, incudeNegative);
            float z = GetRandomFloat(min, max, incudeNegative);

            return new Vector3(x, y, z);
        }
        
        public static float GetRandomFloat(float min, float max, bool incudeNegative = true)
        {
            float randNum = Random.Range(min, max);

            if(incudeNegative)
                randNum *= GetRandomSignFlipper();

            return randNum;
        }

        public static float GetRandomFloat(MinMax minMax, bool includeNegative = true)
        {
            return GetRandomFloat(minMax.Min, minMax.Max, includeNegative);
        }

        public static float GetRandomSignFlipper()
        {
            return (Random.Range(0, 2) == 0 ? -1 : 1);
        }

        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            // Loops through array
            for (int i = (list.Count - 1); i > 0; i--)
            {
                int rnd = Random.Range(0, i);

                T temp = list[i];

                list[i] = list[rnd];
                list[rnd] = temp;
            }

            return list;
        }

        public struct MinMax
        {
            public float Min, Max;
            public MinMax(float min, float max)
            {
                Min = min;
                Max = max;
            }
        }
    }
}
