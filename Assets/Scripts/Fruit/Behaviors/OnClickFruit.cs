﻿using UnityEngine;

public class OnClickFruit : BaseFruitBehavior
{
    private Scaler scaler;

    protected override void AwakeMono()
    {
        gameObject.AddComponent<Scaler>();

        scaler = GetComponent<Scaler>();
    }

    protected override void StartMono()
    {
        duration = .001f;

        TriggerCueOnCallOnly = true;
        TriggerEffectOnCallOnly = true;

        //This can be used to add a the splat, but it is rending on top of the berry...
        // Figure out how to always render below
        TriggerSplatOnCallOnly = true;
    }

    protected override void OnMouseUpMono()
    {
        OnRunBehavior();
    }

    protected override void OnRunBehavior()
    {
        OnRunCue();

        base.OnRunBehavior();
    }

    protected override void OnRunCue()
    {
        scaler.SetNextScale(0, duration);
    }
}