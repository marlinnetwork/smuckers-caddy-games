﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts.Helpers;

public abstract class BaseFruitBehavior : MonoBehaviour
{
    protected Fruit fruit;
    
    protected float duration;

    protected int score = 1;

    protected GameObject effect;
    protected Color currentColor;

    private float nextEffect;
    private float nextCue;

    protected Rand.MinMax randCueTime = new Rand.MinMax(1, 2);
    protected Rand.MinMax randEffectTime = new Rand.MinMax(1, 1);

    protected bool TriggerCueOnCallOnly;
    protected bool TriggerEffectOnCallOnly;
    protected bool TriggerSplatOnCallOnly;

    protected bool SetHasBeenClicked;

    protected Collider[] Colliders;

    private void Awake()
    {
        fruit = transform.GetComponent<Fruit>();
        Colliders = transform.GetComponents<Collider>();

        AwakeMono();
    }

    private void Start()
    {
        StartMono();
        SetCueRun();
        SetEffectRun();
    }

    protected void UpdateEffect(string effectName)
    {
        effect = Messages.AppControllerBus.GetEffectResourceByName(effectName);
    }

    
    private void SetCueRun()
    {
        if (!TriggerCueOnCallOnly)
        {
            nextCue = Rand.GetRandomFloat(randCueTime, false);

            StartCoroutine(RunCue());
        }
    }

    private void SetEffectRun()
    {
        if (!TriggerEffectOnCallOnly)
        {
            nextEffect = Rand.GetRandomFloat(randEffectTime, false);

            if (effect != null)
                StartCoroutine(RunEffect());
        }
    }

    protected virtual void AwakeMono() { }

    protected virtual void StartMono() { }
    
    protected virtual void OnDestroyMono() { }
    
    protected virtual void OnMouseUpMono() {}

    private void OnMouseUp()
    {
        string currentSelectedObjectName = string.Empty;

        try { currentSelectedObjectName = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name; }
        catch { }

        if ((string.IsNullOrEmpty(currentSelectedObjectName) || currentSelectedObjectName != "Button") &&
                UIOverlayController.ValidateOverlayCanAcceptTaps(SetHasBeenClicked))
        {
            if (TriggerSplatOnCallOnly)
                fruit.SplatFruitOnFloor(transform);

            OnMouseUpMono();

        }
    }
    //This is specific to the script after an action
    protected virtual void OnRunBehavior()
    {
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.HideInstructions);
        StopAllCoroutines();
        StartCoroutine(TillDestroy());
    }

    //This is specific to a visual cue before an action
    protected virtual void OnRunCue() { }

    //This is the associated effect before or after an action.
    protected virtual void OnRunEffect() { }

    //Using this currently to remove Gear Menu
    protected void SendMessageToRemoveCurrentOverlay()
    {
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Games, GameMenuOverlay.RemoveCurrent, null);
    }

    public IEnumerator TillDestroy()
    {
        yield return new WaitForSecondsRealtime(duration);

        fruit.RemoveFruit();

        OnDestroyMono();

        StopCoroutine(TillDestroy());
    }

    //These are governed by the instantiated class... if these are run randomly or when it calls
    private IEnumerator RunCue()
    {
        yield return new WaitForSecondsRealtime(nextCue);

        OnRunCue();

        StopCoroutine(RunCue());

        SetCueRun();
    }

    private IEnumerator RunEffect()
    {
        yield return new WaitForSecondsRealtime(nextEffect);

        OnRunEffect();

        StopCoroutine(RunEffect());

        SetEffectRun();
    }
}