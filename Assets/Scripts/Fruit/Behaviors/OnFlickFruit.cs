﻿using UnityEngine;
using Assets.Scripts.Models;
using Assets.Scripts.Effects;
using System.Linq;

public class OnFlickFruit : BaseFruitBehavior
{
    private Rigidbody rig;
    private float flickForce;
    private string Category;
    private GameObject inst;

    protected override void AwakeMono()
    {
        rig = transform.GetComponent<Rigidbody>();
    }

    protected override void StartMono()
    {
        duration = 1f;
        flickForce = 15f;

        Category = Messages.GamesControllerBus.GetNextAvailableCategoryForActiveGame();

        TriggerCueOnCallOnly = true;
        SetHasBeenClicked = true;

        UpdateEffect("UndulateIcon");
    }

    protected override void OnDestroyMono()
    {
        FunFactsGameModel model = new FunFactsGameModel()
        {
            Category = Category
        };

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Games, GameMenuOverlay.Load, model);
        Messages.UIOverlayControllerBus.SetHasBeenClicked(false);
    }

    protected override void OnMouseUpMono()
    {
        OnRunBehavior();
        Messages.GamesControllerBus.UpdateActiveGameScore(score);
    }

    protected override void OnRunBehavior()
    {
        rig.AddForce(Vector3.forward * (flickForce), ForceMode.Acceleration);
        rig.AddTorque(Vector3.forward * (flickForce / 2), ForceMode.Force);

        SendMessageToRemoveCurrentOverlay();

        base.OnRunBehavior();
    }

    protected override void OnRunEffect()
    {
        //In this case we dont want the icons to display if the fruit is not visible,
        // So we are using the fruits colliders are an indicator to show icons.
        // NOTE: These colliders are enable/disabled fron vuforia - DefaultTrackableEventHandler class
        if (Colliders.Any(A => A.enabled))
        {
            inst = Instantiate(effect, transform);

            inst.transform.localPosition += new Vector3(.05f, 0, 0);
            inst.transform.localScale = new Vector3(.5f, .5f, .5f);

            IEffect e = inst.GetComponent<IEffect>();

            UndulateModel model = (UndulateModel)e.GetModelState();
            model.Category = Category.Replace(" ", "");

            e.SetModelState(model);
            e.Activate();
        }
    }
}