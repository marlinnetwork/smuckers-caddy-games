﻿using UnityEngine;
using Assets.Scripts.Effects;


public class OnExplodeFruit : BaseFruitBehavior
{
    private Scaler scaler;

    protected override void AwakeMono()
    {
        gameObject.AddComponent<Scaler>();

        scaler = GetComponent<Scaler>();
    }

    protected override void StartMono()
    {
        duration = .125f;

        TriggerCueOnCallOnly = true;
        TriggerEffectOnCallOnly = true;

        UpdateEffect("explode");
    }

    protected override void OnMouseUpMono()
    {
        OnRunBehavior();
        Messages.GamesControllerBus.UpdateActiveGameScore(score);
    }

    protected override void OnRunBehavior()
    {
        OnRunCue();
        OnRunEffect();

        //This has been moved to from OnDestroyMono to update the game score when the player clicked
        base.OnRunBehavior();
    }

    protected override void OnRunCue()
    {
        scaler.SetNextScale(0, duration);
    }

    protected override void OnRunEffect()
    {
        GameObject inst = Instantiate(effect, transform.parent.transform);
        inst.transform.localPosition = transform.localPosition;

        inst.GetComponent<IColor>().SetColor(fruit.Color);

        IEffect e = inst.GetComponent<IEffect>();

        e.Activate();
    }
}