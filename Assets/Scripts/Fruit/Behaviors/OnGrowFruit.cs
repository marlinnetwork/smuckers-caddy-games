﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Models;

public class OnGrowFruit : BaseFruitBehavior
{
    private Scaler scaler;
    private Mover mover;

    private float scaleFactor;

    protected override void AwakeMono()
    {
        gameObject.AddComponent<Scaler>();
        gameObject.AddComponent<Mover>();

        scaler = GetComponent<Scaler>();
        mover = GetComponent<Mover>();
    }

    protected override void StartMono()
    {
        scaleFactor = 10f;
        duration = .25f;

        TriggerEffectOnCallOnly = true;
        SetHasBeenClicked = true;
    }

    protected override void OnDestroyMono()
    {
        ConversationStartersGameModel model = new ConversationStartersGameModel()
        {
            SplatColor = fruit.Color
        };

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Games, GameMenuOverlay.Load, model);
        Messages.UIOverlayControllerBus.SetHasBeenClicked(false);
    }

    protected override void OnMouseUpMono()
    {
        OnRunBehavior();
        Messages.GamesControllerBus.UpdateActiveGameScore(score);
    }

    protected override void OnRunBehavior()
    {
        scaler.SetNextScale(scaleFactor, duration);
        mover.SetToPosition(transform.localPosition);
        mover.SetNextPosition(Camera.main.transform.position, Vector3.zero, Vector3.zero, duration);

        SendMessageToRemoveCurrentOverlay();

        base.OnRunBehavior();
    }

    protected override void OnRunCue()
    {
        scaler.SetNextScale(1.25f, .25f, true);
    }
}