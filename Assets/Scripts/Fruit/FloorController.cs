﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Models;
using Assets.Scripts.Effects;


public class FloorController : MonoBehaviour
{
    private GameObject currenteffect;
    private Color currentColor;


    private void Start()
    {
        currenteffect = Messages.AppControllerBus.GetEffectResourceByName("splat");
    }

    public void UpdateEffect(Color color)
    {
        currentColor = color;
    }

    public void SplatFruitOnFloor(Transform transform)
    {
        GameObject inst = Instantiate(currenteffect, this.transform);
        
        inst.transform.localPosition = new Vector3(transform.localPosition.x / this.transform.localScale.x, transform.localPosition.y / this.transform.localScale.y, -.5f);
        inst.GetComponentInChildren<IColor>().SetColor(currentColor);

        IEffect e = inst.GetComponentInChildren<IEffect>();

        e.Activate();
    }
}
