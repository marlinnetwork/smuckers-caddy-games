﻿using UnityEngine;
using Assets.Scripts.Helpers;

public class Fruit : MonoBehaviour
{
    private const int removeForce = 1000;

    private Rigidbody rig;
    private FruitsController controller;

    // creating a +/- variation in the fruit using scalerRangePercent
    private float scaler;
    private int atRest;
    private Collider[] colliders;

    private Transform floor;

    private string activeGame;
    public string ActiveGame { get => activeGame; set => activeGame = value; }

    private Color color;
    public Color Color { get => color; set => color = value; }

    //these may be customized per fruit....
    [SerializeField]
    public float explosionRadius;
    [SerializeField]
    public float explosionPower;
    [SerializeField]
    public float upwardsModifier;
    [SerializeField]
    public int scalerRangePercent;

    [Space(10)]
    [SerializeField]
    public float gravityScale = 1.0f;
    [SerializeField]
    public float globalGravity = -9.81f;

    private void Awake()
    {
        scaler = Rand.GetRandomFloat(0, scalerRangePercent) / 100;
        rig = GetComponent<Rigidbody>();
        controller = GetComponentInParent<FruitsController>();
        colliders = GetComponents<Collider>();
        activeGame = string.Empty;

        Physics.IgnoreLayerCollision(1, 0);
    }

    private void Start()
    {
        transform.localScale += (transform.localScale * scaler);
        rig.mass += (rig.mass * scaler);

        transform.localRotation = Random.rotation;

        rig.useGravity = false;

        atRest = 0;

        gameObject.AddComponent(GetFruitBehavior(activeGame));
    }

    private void Update()
    {
        if (Vector3.Distance(transform.localPosition, Camera.main.transform.localPosition) > 5f)
            RemoveFruit();
    }

    private void FixedUpdate()
    {
        if (atRest < removeForce && gameObject.activeInHierarchy)
            rig.AddForce(globalGravity * gravityScale * Vector3.up, ForceMode.Acceleration);
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Lid")
        {
            foreach (Collider c in colliders)
                Physics.IgnoreCollision(col.collider, c);
        }

        if (col.gameObject.name.ToLower() == "floor")
            floor = col.transform;
    }

    private void OnCollisionStay(Collision col)
    {
        if (col.gameObject.name.ToLower() == "floor")
        {
            atRest++;
        }
    }

    public void LaunchFruit()
    {
        Vector3 randomPos = Rand.GetRandom(3f, 4f);
        
        rig.AddExplosionForce(explosionPower, transform.localPosition + randomPos, explosionRadius, upwardsModifier);
    }

    public void RemoveFruit()
    {
        controller.RemoveFruit(transform.gameObject);
    }

    public void SplatFruitOnFloor(Transform transform)
    {
        controller.SplatFruitOnFloor(transform);
    }

    public void UpdateBehavior(string newGame)
    {
        Destroy(GetComponent(GetFruitBehavior(activeGame)));

        gameObject.AddComponent(GetFruitBehavior(newGame));

        activeGame = newGame;
    }

    private System.Type GetFruitBehavior(string game)
    {
        switch (game)
        {
            case "fruitsmash":
                return typeof(OnExplodeFruit);
            case "funfacts":
                return typeof(OnFlickFruit);
            case "conversationstarters":
                return typeof(OnGrowFruit);
            default:
                return typeof(OnClickFruit);
        }
    }
}