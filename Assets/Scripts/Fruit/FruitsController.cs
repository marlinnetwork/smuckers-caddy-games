﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FruitsController : MonoBehaviour
{
    // So After a fruit is spawned, depending on the game we will add the appropriate script
    [SerializeField]
    public int totalFruit;

    [Range(0f, 5f)]
    [SerializeField]
    public float initialFruitSpawnDelay;

    private float fruitSpawnDelay;

    [Range(0f, 1f)]
    [SerializeField]
    public float perFruitSpawnDelayMin;

    [Range(0f, 1f)]
    [SerializeField]
    public float perFruitSpawnDelayMax;

    private GameObject currentFruit;
    private List<GameObject> fruitList;
    private string activeGame;
    private Color color;
    private bool spawnFruitState;

    private FloorController _floorController;

    public void Awake()
    {
        fruitList = new List<GameObject>();
        activeGame = string.Empty;
        _floorController = GetComponentInChildren<FloorController>();
    }

    public void UpdateSpawnableFruit(GameObject spawnable, Color color)
    {
        currentFruit = spawnable;
        this.color = color;

        _floorController.UpdateEffect(color);
    }

    public void UpdateState(Assets.Scripts.Models.PCStateModel model)
    {
        fruitSpawnDelay = initialFruitSpawnDelay;

        activeGame = model.ActiveGame;

        //Adding model.ControllerState == PCControllerState.NotVisiblePlayingGame to spawn fruit evertime the game is playing or selectable
        if (model.ControllerState == PCControllerState.Selectable || model.ControllerState == PCControllerState.InActivate || model.ControllerState == PCControllerState.NotVisiblePlayingGame)
            RemoveAllFruit();
        else
            UpdateAllFruitBehaviors();

        //We can only spawn fruit while selectable or playing game
        spawnFruitState = (model.ControllerState == PCControllerState.PlayingGame || model.ControllerState == PCControllerState.Selectable);

        if (spawnFruitState)
        {
            StopCoroutine(SpawnFruits());
            StartCoroutine(SpawnFruits());
        }
    }

    //Coroutine to spawn fruit
    public IEnumerator SpawnFruits()
    {
        if(fruitSpawnDelay > 0)
        {
            yield return new WaitForSecondsRealtime(fruitSpawnDelay);

            fruitSpawnDelay = 0;
        }

        if(fruitList.Count < totalFruit)
            StartCoroutine(SpawnFruit());
    }

    public void RemoveAllFruit()
    {
        fruitList.ForEach(FE => { Destroy(FE); });
        fruitList = new List<GameObject>();
    }

    public void UpdateAllFruitBehaviors()
    {
        fruitList.ForEach(FE =>
        {
            FE.GetComponent<Fruit>().UpdateBehavior(activeGame);
        });
    }

    public void RemoveFruit(GameObject obj)
    {
        Destroy(obj);
        fruitList.Remove(obj);

        if (gameObject.activeInHierarchy)
            StartCoroutine(SpawnFruits());
    }

    public void SplatFruitOnFloor(Transform transform)
    {
        _floorController.SplatFruitOnFloor(transform);
    }

    IEnumerator SpawnFruit()
    {
        yield return new WaitForSecondsRealtime(Random.Range(perFruitSpawnDelayMin, perFruitSpawnDelayMax));

        if (fruitList.Count < totalFruit && spawnFruitState)
        {
            GameObject inst = Instantiate(currentFruit, transform);
            var id = Time.deltaTime.ToString();
            inst.name = currentFruit.name + ((id.Length >=5) ? id.Substring(0, 5) : id);

            Fruit f = inst.GetComponent<Fruit>();

            f.ActiveGame = activeGame;
            f.Color = color;

            f.LaunchFruit();

            fruitList.Add(inst);

            StartCoroutine(SpawnFruit());
        }
        else
        {
            StopCoroutine(SpawnFruit());
        }
    }
}