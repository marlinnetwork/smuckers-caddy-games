﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class LightingEstimation : MonoBehaviour
{
    private StateManager stateManager;
    private IlluminationManager lightingManager;

    public bool enableFruitLighting = true;

    private float lightValue;

    private const float MAX_AMBIENT_INTENSITY = 3000.0f;

    private const float MAX_POINT_LIGHT_INTENSITY = 0.75f;

    private Light berryLighting;


    // Start is called before the first frame update
    void Start()
    {
        stateManager = TrackerManager.Instance.GetStateManager();
        lightingManager = stateManager.GetIlluminationManager();
        berryLighting = GetComponentInChildren<Light>();
    }

    void FixedUpdate()
    {

        if(enableFruitLighting && lightingManager.AmbientIntensity != null)
        {
            lightValue = (float)((MAX_POINT_LIGHT_INTENSITY / MAX_AMBIENT_INTENSITY) * (lightingManager.AmbientIntensity) + 0.01f);
        }
    }
}
