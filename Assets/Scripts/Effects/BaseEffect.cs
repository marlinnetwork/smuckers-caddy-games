﻿using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    public abstract class BaseEffect : MonoBehaviour
    {
        private BaseEffectModel model;

        protected BaseEffectModel GetModel() { return model;  }

        private bool _startCountDownToDestroy;

        public void StartCountDownToDestroy()
        {
            _startCountDownToDestroy = true;
        }

        private float _currentEffectTime;

        public float CurrentEffectTime
        {
            get { return _currentEffectTime; }
        }

        private void Awake()
        {
            _startCountDownToDestroy = false;
            _currentEffectTime = 0f;

            AwakeMono();
        }

        private void Update()
        {
            UpdateMono();
            
            if (_startCountDownToDestroy)
            {
                _currentEffectTime += Time.deltaTime;

                if (GetDestroyCondition())
                    Destroy(gameObject);
            }
        }

        private void FixedUpdate()
        {
            FixedUpdateMono();
        }

        protected virtual void AwakeMono() { }

        protected virtual void UpdateMono() { }

        protected virtual void FixedUpdateMono() { }

        protected virtual bool GetDestroyCondition() { return false; }
    }
}
