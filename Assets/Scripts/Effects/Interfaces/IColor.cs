﻿namespace Assets.Scripts.Effects
{
    public interface IColor
    {
        void SetColor(UnityEngine.Color color);

        //This is afterActivation
        void UpdateColor(UnityEngine.Color color);
    }
}
