﻿
namespace Assets.Scripts.Effects
{
    public interface IEffect
    {
        void Activate();

        //This is an override if you want to change some of the effect from the '
        //instantiating game ojbect
        void SetModelState(Models.BaseEffectModel model);

        Models.BaseEffectModel GetModelState();
    }
}
