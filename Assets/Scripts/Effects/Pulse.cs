﻿using UnityEngine;

namespace Assets.Scripts.Effects
{
    public class Pulse : BaseEffect, IEffect, IColor
    {
        [SerializeField]
        public Models.PulseModel model;

        protected ColorUpdater _colorUpdater;
        protected Scaler _scaler;
        protected SolidMaterialAlphar _alphar;
        
        private BoxCollider _lidCollider;      

        //Overrides
        protected override void AwakeMono()
        {
            _lidCollider = GetComponentInParent<BoxCollider>();

            _colorUpdater = GetComponent<ColorUpdater>();
            _scaler = GetComponent<Scaler>();
            _alphar = GetComponent<SolidMaterialAlphar>();
        }

        protected override void UpdateMono() {}

        protected override bool GetDestroyCondition()
        {
            return (CurrentEffectTime > model.Duration || !_lidCollider.enabled);
        }

        //IEffect Interface
        public void Activate()
        {
            _colorUpdater.UpdateColor(model.Color);

            if (model.ScaleToParent)
                _scaler.SetToScale(transform.localScale);

            _scaler.SetNextScale(model.ScaleFactor, model.Duration);

            _alphar.SetToAlpha(model.AlphaStart);
            _alphar.SetNextAlpha(model.AlphaTo, model.Duration);

            StartCountDownToDestroy();
        }

        public void SetModelState(Models.BaseEffectModel model)
        {
            this.model = (Models.PulseModel)model;
        }

        public Models.BaseEffectModel GetModelState()
        {
            return model;
        }

        //IColor Interface
        public void SetColor(Color color)
        {
            model.Color = color;
        }

        public void UpdateColor(Color color)
        {
            if (_colorUpdater != null)
                _colorUpdater.UpdateColor(color);
        }
    }
}