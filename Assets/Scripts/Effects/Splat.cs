﻿using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    public class Splat : BaseEffect, IEffect, IColor
    {
        [SerializeField]
        public SplatModel model;

        private MeshRenderer _rend;

        protected ColorUpdater _colorUpdater;
        protected Scaler _scaler;
        protected SolidMaterialAlphar _alphar;

        [SerializeField]

        //Overrides
        protected override void AwakeMono()
        {
            _rend = GetComponent<MeshRenderer>();

            _colorUpdater = GetComponent<ColorUpdater>();
            _scaler = GetComponent<Scaler>();
            _alphar = GetComponent<SolidMaterialAlphar>();
        }

        protected override bool GetDestroyCondition()
        {
            return (CurrentEffectTime > model.Duration);
        }

        //IEffect Interface
        public void Activate()
        {
            _rend.material = model.Splats[Random.Range(0, (model.Splats.Length))];

            _scaler.SetToScale(new Vector3(model.ScaleStart, model.ScaleStart, model.ScaleStart));

            _colorUpdater.UpdateColor(model.Color);

            _scaler.SetNextScale(model.ScaleFactor, model.Duration);

            _alphar.SetToAlpha(model.AlphaStart);
            _alphar.SetNextAlpha(model.AlphaTo, model.Duration);

            transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));

            StartCountDownToDestroy();
        }

        public BaseEffectModel GetModelState()
        {
            return model;
        }

        public void SetModelState(BaseEffectModel model)
        {
            this.model = (Models.SplatModel)model;
        }
        //IColor Interface
        public void SetColor(Color color)
        {
            model.Color = color;
        }

        public void UpdateColor(Color color)
        {
            if (_colorUpdater != null)
                _colorUpdater.UpdateColor(color);
        }
    }
}