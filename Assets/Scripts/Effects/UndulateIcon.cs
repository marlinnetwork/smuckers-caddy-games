﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Effects
{
    public class UndulateIcon : BaseEffect, IEffect
    {
        [SerializeField]
        public Material[] materials;

        [SerializeField]
        public Models.UndulateModel model;

        private Mover _mover;
        private Scaler _scaler;
        private SolidMaterialAlphar _alphar;
        private MeshRenderer _rend;

        private Camera uiCamera;
        private IDictionary<string, Material> materialDictionary;

        protected override void AwakeMono()
        {
            _mover = GetComponent<Mover>();
            _scaler = GetComponent<Scaler>();
            _alphar = GetComponent<SolidMaterialAlphar>();
            _rend = GetComponent<MeshRenderer>();

            materialDictionary = new Dictionary<string, Material>();

            foreach(Material m in materials)
            {
                materialDictionary.Add(LoadDictionaryItem(m));
            }
        }

        protected override void UpdateMono()
        {
            transform.LookAt(Camera.main.transform.position);
            transform.rotation = Camera.main.transform.rotation;
        }

        protected override bool GetDestroyCondition()
        {
            return (CurrentEffectTime > model.Duration);
        }

        //Interface
        public void Activate()
        {
            _rend.material = materialDictionary[model.Category];

            float duration = (model.PingPong) ?
                model.Duration / 2 :
                model.Duration;

            _scaler.SetNextScale(model.ScaleFactor, duration, true);

            _alphar.SetToAlpha(model.AlphaStart);
            _alphar.SetNextAlpha(model.AlphaTo, duration, model.PingPong);

            StartCountDownToDestroy();
        }

        public void SetModelState(Models.BaseEffectModel model)
        {
            this.model = (Models.UndulateModel)model;
        }

        public Models.BaseEffectModel GetModelState()
        {
            return model;
        }

        private KeyValuePair<string, Material> LoadDictionaryItem(Material mat)
        {
            return new KeyValuePair<string, Material>(mat.name.ToUpper(), mat);
        }
    }
}
