﻿using UnityEngine;

namespace Assets.Scripts.Effects
{
    public class Explode : BaseEffect, IEffect, IColor
    {
        [SerializeField]
        public Models.ExplodeModel model;

        protected ColorUpdater _colorUpdater;
        protected Scaler _scaler;
        protected SolidMaterialAlphar _alphar;

        //Overrides
        protected override void AwakeMono()
        {
            _colorUpdater = GetComponent<ColorUpdater>();
            _scaler = GetComponent<Scaler>();
            _alphar = GetComponent<SolidMaterialAlphar>();
        }

        protected override void UpdateMono()
        {
            transform.LookAt(Camera.main.transform.position);
            transform.rotation = Camera.main.transform.rotation;
        }

        protected override bool GetDestroyCondition()
        {
            return (CurrentEffectTime > model.Duration);
        }

        //IEffect Interface
        public void Activate()
        {
            _colorUpdater.UpdateColor(model.Color);

            if (model.ScaleToParent)
                _scaler.SetToScale(transform.localScale);

            _scaler.SetNextScale(model.ScaleFactor, model.Duration);

            _alphar.SetToAlpha(model.AlphaStart);
            _alphar.SetNextAlpha(model.AlphaTo, model.Duration);

            StartCountDownToDestroy();
        }

        public void SetModelState(Models.BaseEffectModel model)
        {
            this.model = (Models.ExplodeModel)model;
        }

        public Models.BaseEffectModel GetModelState()
        {
            return model;
        }

        //IColor Interface
        public void SetColor(Color color)
        {
            model.Color = color;
        }

        public void UpdateColor(Color color)
        {
            if (_colorUpdater != null)
                _colorUpdater.UpdateColor(color);
        }
    }
}