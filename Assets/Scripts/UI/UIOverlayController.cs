﻿using UnityEngine;
using System.Collections.Generic;

public sealed partial class UIOverlayController : MonoBehaviour
{
    private InstructionsController _instructionsController;
    private GameMenuOverlayController _gameMenuOverlayController;

    private bool _hasBeenClicked { get; set; }

    private void Awake()
    {
        _instructionsController = GetComponentInChildren<InstructionsController>();
        _gameMenuOverlayController = GetComponentInChildren<GameMenuOverlayController>();
    }

    //payload will be something else but for now
    // This whole messaging structure will change
    // dependent on the learning for games and menus
    // but for now...
    public void Message(UIMessageFor messageFor, object messageType, object payload)
    {
        switch(messageFor)
        {
            case UIMessageFor.Instructions:
                _instructionsController.Message((UIInstructions)messageType, payload);
                break;
            case UIMessageFor.Menus:
            case UIMessageFor.Games:
                _gameMenuOverlayController.Message((GameMenuOverlay)messageType, payload);
                break;
        }
    }
}