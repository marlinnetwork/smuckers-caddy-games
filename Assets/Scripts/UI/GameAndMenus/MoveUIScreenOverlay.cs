﻿using UnityEngine;
using UnityEngine.UI;

public class MoveUIScreenOverlay : MonoBehaviour
{
    private Vector3 outPosition;
    private Vector3 inPosition;
    private bool UseBumpPoints;

    private RectTransform rectTransform;
    private Mover mover;



    private void Awake()
    {
        mover = GetComponent<Mover>();

        rectTransform = ((RectTransform)transform);
    }

    public void SetPositions(Assets.Scripts.Models.BaseContentOverlay baseContentOverlay)
    {
        UseBumpPoints = baseContentOverlay.UseBumpPoints;

        outPosition = (baseContentOverlay.StartAtOrigin) ?
            ((RectTransform)transform.parent.transform).localPosition :
            GetVectorForRelativeLocation(baseContentOverlay.OutLocation);

        inPosition = (baseContentOverlay.StartAtOrigin) ?
            GetVectorForRelativeLocation(baseContentOverlay.OutLocation) :
            ((RectTransform)transform.parent.transform).localPosition;

        mover.SetToPosition(outPosition);
    }

    public void Toggle(float transitionTime)
    {
        var nextLP = (mover.transform.localPosition == inPosition) ?
                        outPosition : inPosition;

        if(UseBumpPoints)
            mover.SetNextPosition(nextLP, SetBumpPoints(nextLP, 8), SetBumpPoints(nextLP, 10), transitionTime);
        else
            mover.SetNextPosition(nextLP, Vector3.zero, Vector3.zero, transitionTime);
    }

    private Vector3 GetVectorForRelativeLocation(RelativeLocation location)
    {
        Vector3 newLoc = Vector3.zero;

        switch (location)
        {
            case RelativeLocation.Top:
                newLoc = new Vector3(0, rectTransform.rect.height, 0); ;
                break;
            case RelativeLocation.Right:
                newLoc = new Vector3(rectTransform.rect.width, 0, 0);
                break;
            case RelativeLocation.Bottom:
                newLoc = new Vector3(0, -rectTransform.rect.height, 0);
                break;
            case RelativeLocation.Left:
                newLoc = new Vector3(-rectTransform.rect.width, 0, 0);
                break;
            case RelativeLocation.TopRight:
                newLoc = new Vector3(rectTransform.rect.width, rectTransform.rect.height, 0); ;
                break;
            case RelativeLocation.BottomRight:
                newLoc = new Vector3(rectTransform.rect.width, -rectTransform.rect.height, 0);
                break;
            case RelativeLocation.BottomLeft:
                newLoc = new Vector3(-rectTransform.rect.width, -rectTransform.rect.height, 0);
                break;
            case RelativeLocation.TopLeft:
                newLoc = new Vector3(-rectTransform.rect.width, rectTransform.rect.height, 0); ;
                break;
        }

        return newLoc;
    }

    private Vector3 SetBumpPoints(Vector3 nextPosition, float bumpSize)
    {
        Vector3 bump = new Vector3()
        {
            x = (nextPosition.x / bumpSize) * -1,
            y = (nextPosition.y / bumpSize) * -1,
            z = (nextPosition.z / bumpSize) * -1
        };

        return bump;
    }
}
