﻿using UnityEngine;
using UnityEngine.Analytics;
using Assets.Scripts.Models;
using System.Collections;
using Assets.Scripts.Menus.Overlays;

public sealed partial class GameMenuOverlayController : MonoBehaviour
{
    private IContentOverlay content;

    public void Message(GameMenuOverlay type, object obj)
    {
        switch(type)
        {
            case GameMenuOverlay.Load:
                Load((BaseContentOverlay)obj);
                break;
            case GameMenuOverlay.RemoveCurrent:
                Remove();
                break;
        }
    }

    private void Load(BaseContentOverlay contentOverlay)
    {
        //get the component from the service
        GameObject overlay = Messages.UIOverlayControllerBus.GetOverlayResourceByName(contentOverlay.ResourceName);

        //Instatiate The overlay
        GameObject contentObj = Instantiate(overlay, transform);

        content = contentObj.GetComponent<IContentOverlay>();

        //var baseContent = contentObj.GetComponent<BaseContent>();
        //var analyticsName = baseContent.GetAnalyticsName();

        //if(!string.IsNullOrEmpty(analyticsName))
        //{
        //    AnalyticsEvent.ScreenVisit(analyticsName);
        //}

        content.UpdateModel(contentOverlay);

        StartCoroutine(StartOverlay());
    }

    private void Remove()
    {
        if(content != null)
            content.Toggle();
    }

    IEnumerator StartOverlay()
    {
        yield return new WaitForEndOfFrame();

        content.Toggle();

        StopCoroutine(StartOverlay());
    }
}
