﻿using Assets.Scripts.Models;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class FunFactsEndContent : EndContent
{
    private FunFactsSettingsModel model;

    protected override void StartMono()
    {
        model = (FunFactsSettingsModel)Messages.GamesControllerBus.GetActiveGameSettings();
    }
       
    public void OnClickChangeGamePreferences()
    {
        Analytics.CustomEvent("click", new Dictionary<string, object>() {
            { "element", "Change Game Preferences" },
            { "game", Messages.GamesControllerBus.GetActiveGame() }
        });

        if (model == null)
            model = (FunFactsSettingsModel)Messages.GamesControllerBus.GetActiveGameSettings();

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, model);

        Toggle();
    }
}