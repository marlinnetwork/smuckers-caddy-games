﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Models;

public class FunFactsGameContent : BaseContent
{
    [SerializeField]
    public Sprite[] Categories;

    [SerializeField]
    public Image CategoryImage;

    [SerializeField]
    public Text Category;

    [SerializeField]
    public Text FunFact;

    [SerializeField]
    public string CopyToReplace = "{{fill in the blank}}";

    [SerializeField]
    public Color StartColor;

    [SerializeField]
    public Color EndColor;

    [Range(.01f,5f)]
    [SerializeField]
    public float DurationOfBlink;

    private IDictionary<string, Sprite> categoryDictionary;

    private FunFactModel model;

    private string startColorHex;
    private string endColorHex;
    private string nextColorHex;

    private string PlaceHolderCopy;

    protected override void AwakeMono()
    {
        categoryDictionary = new Dictionary<string, Sprite>();
        
        foreach(Sprite s in Categories)
        {
            categoryDictionary.Add(LoadDictionaryItem(s));
        }
    }

    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        SetBaseColor(contentOverlay);

        string category = ((FunFactsGameModel)contentOverlay).Category;
        model = Messages.GamesControllerBus.GetNextFunFactByCategoryName(category);

        CategoryImage.sprite = categoryDictionary["ICON_" + category.Replace(" ","") + "_COLOR"];
        Category.text = model.Category;

        startColorHex = "#" + ColorUtility.ToHtmlStringRGBA(StartColor);
        endColorHex = "#" + ColorUtility.ToHtmlStringRGBA(EndColor);

        nextColorHex = startColorHex;

        for (int i = 0; i < model.Answer.Length; i++)
        {
            PlaceHolderCopy += "_";
        }

        UpdateFunFactCopyToReplaceText(PlaceHolderCopy, GetReplacementColor());

        StartCoroutine(BlinkSpace());
    }

    protected override void ToggleContent()
    {
        if (activate)
        {
            //anything that needs to aminate or function here
            activate = false;
        }
        else
        {
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new GearMenuModel(baseColor, baseResourceName));

            StartCoroutine(TillDestroy());
        }
    }

    public void OnSwipeOrClick()
    {
        UpdateFunFactCopyToReplaceText(model.Answer, endColorHex);

        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private KeyValuePair<string, Sprite> LoadDictionaryItem(Sprite sprite)
    {
        return new KeyValuePair<string, Sprite>(sprite.name.ToUpper(), sprite);
    }

    private string GetReplacementColor()
    {
        string color = nextColorHex;

        nextColorHex = (nextColorHex == startColorHex) ? endColorHex : startColorHex;

        return color;
    }

    private void UpdateFunFactCopyToReplaceText(string text, string color)
    {
        FunFact.text = model.FunFact.Replace(CopyToReplace, "<b><color=" + color + ">" + text + "</color></b>");
    }

    IEnumerator BlinkSpace()
    {
        yield return new WaitForSecondsRealtime(DurationOfBlink);

        UpdateFunFactCopyToReplaceText(PlaceHolderCopy, GetReplacementColor());

        StartCoroutine(BlinkSpace());
    }
}