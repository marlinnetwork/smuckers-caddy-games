﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using System.Collections.Generic;
using System.Linq;

public class FunFactsSettingsContent : SettingsContent
{
    [SerializeField]
    public GameObject PopCulture;

    [SerializeField]
    public GameObject History;

    [SerializeField]
    public GameObject Food;

    [SerializeField]
    public GameObject Science;

    [SerializeField]
    public Text AtLeastOne;

    private IDictionary<string, Toggle> toggleDictionary;

    //we override from the base to get this monobehaviour method;
    protected override void AwakeMono()
    {
        toggleDictionary = new Dictionary<string, Toggle>();

        toggleDictionary.Add(LoadDictionaryItem(PopCulture));
        toggleDictionary.Add(LoadDictionaryItem(History));
        toggleDictionary.Add(LoadDictionaryItem(Food));
        toggleDictionary.Add(LoadDictionaryItem(Science));
    }

    //Update The toggle states and animations
    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        FunFactsSettingsModel model = (FunFactsSettingsModel)contentOverlay;

        if (model.DisabledFunFacts != null)
        {
            foreach (string element in model.DisabledFunFacts)
            {
                toggleDictionary[element].animator.SetTrigger("Toggle");
                toggleDictionary[element].isOn = false;
            }
        }

        SetBaseColor(contentOverlay);
    }

    public override void OnClickContinue()
    {
        FunFactsSettingsModel model = new FunFactsSettingsModel();
        string[] disabledFunFacts = toggleDictionary.Where(W => !W.Value.isOn).Select(S => S.Key).ToArray();

        model.DisabledFunFacts = (disabledFunFacts != null) ?
            disabledFunFacts :
            new string[] { };

        if (model.DisabledFunFacts.Length < toggleDictionary.Count())
        {
            Messages.GamesControllerBus.SetActiveGameSettings(model);
            Messages.GamesControllerBus.SetCurrentActiveGame(Messages.GamesControllerBus.GetActiveGame());

            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new FunFactsSplashModel());

            base.OnClickContinue();
        }
        else
            AtLeastOne.text = Instructions.AtLeastOnCategory.Replace("{{COPY}}", "Trivia");
    }

    public override void OnClickBackOrXButton()
    {
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new GearMenuModel(baseColor, baseResourceName));

        Toggle();
    }

    private KeyValuePair<string, Toggle> LoadDictionaryItem(GameObject obj)
    {
        return new KeyValuePair<string, Toggle>(obj.name.ToLower(), obj.GetComponentInChildren<Toggle>());
    }
}