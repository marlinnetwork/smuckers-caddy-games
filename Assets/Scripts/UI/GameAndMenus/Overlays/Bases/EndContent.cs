﻿using Assets.Scripts.Models;
using UnityEngine;
using UnityEngine.Analytics;
using System.Collections.Generic;

public abstract class EndContent : BaseContent
{
    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        SetBaseColor(contentOverlay);

        FruitType otherFruitType = Messages.AppControllerBus.GetActivePCFruitType() == FruitType.Strawberry ? FruitType.Grape : FruitType.Strawberry;
    }

    public void OnClickPlayADifferentGame()
    {
        Analytics.CustomEvent("click", new Dictionary<string, object>() {
            { "element", "Play A Difference Game" },
            { "game", Messages.GamesControllerBus.GetActiveGame() }
        });

        Messages.GamesControllerBus.SetCurrentActiveGame(string.Empty);
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new GameSelectModel());

        Toggle();
    }

    public virtual void OnEndGame()
    {
        Analytics.CustomEvent("click", new Dictionary<string, object>() {
            { "menu", "Exit Gaming Mode" },
            { "game", Messages.GamesControllerBus.GetActiveGame() }
        });

        Messages.GamesControllerBus.SetCurrentActiveGame(string.Empty);
        Messages.AppControllerBus.SetActivePCState(PCControllerState.Selectable);

        Toggle();
    }

    public override void OnClickBackOrXButton()
    {
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new GearMenuModel(baseColor, baseResourceName));

        base.OnClickBackOrXButton();
    }
}
