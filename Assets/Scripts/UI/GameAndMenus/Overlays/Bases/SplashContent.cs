﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Models;

public abstract class SplashContent : BaseContent
{
    [SerializeField]
    public Text countDownText;

    [SerializeField]
    public int startCountDownDelaySeconds;

    [SerializeField]
    public int countDownSeconds;

    [SerializeField]
    public string concatenator;

    [SerializeField]
    public int concatenatorCount;
    
    private float decrementController;

    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        countDownText.text = string.Empty;
        decrementController = 0;

        SetBaseColor(contentOverlay);
    }

    protected override void ToggleContent()
    {
        if(activate)
        {
            Messages.GamesControllerBus.SetCurrentActiveGame(baseResourceName);

            StartCoroutine(StartCountDown());

            activate = false;
        }
        else
        {
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new GearMenuModel(baseColor, baseResourceName));
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.SetBackgroundCopy, GetUIInstructions());
            
            AnalyticsEvent.GameStart(new Dictionary<string, object>() {
                {"game_start", baseResourceName}
            });
            
            StartCoroutine(TillDestroy());
        }
    }

    protected string GetUIInstructions()
    {
        return Messages.GamesControllerBus.GetActiveGameInstructions();
    }

    protected FruitType GetActivePCFruitType()
    {
        return Messages.AppControllerBus.GetActivePCFruitType();
    }

    protected IEnumerator StartCountDown()
    {
        yield return new WaitForSecondsRealtime(startCountDownDelaySeconds);

        StartCoroutine(DecrementCountDown());

        StopCoroutine(StartCountDown());
    }

    protected IEnumerator DecrementCountDown()
    {
        float delay = 1f / (1f + concatenatorCount);

        yield return new WaitForSecondsRealtime(delay);

        decrementController += delay;

        countDownText.text += (countDownSeconds > 0 && decrementController == delay) ?
             countDownSeconds.ToString() :
             concatenator;

        if (decrementController == 1f)
        {
            countDownSeconds--;
            decrementController = 0f;
        }

        if (countDownSeconds > 0)
            StartCoroutine(DecrementCountDown());
        else
        {
            StopCoroutine(DecrementCountDown());
            Toggle();
        }
    }
}
