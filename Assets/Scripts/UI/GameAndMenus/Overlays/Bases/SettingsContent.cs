﻿public abstract class SettingsContent : BaseContent
{
    public virtual void OnClickContinue()
    {
        Toggle();
    }
}