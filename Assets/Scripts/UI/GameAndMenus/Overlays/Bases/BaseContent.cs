﻿using System.Collections;
using UnityEngine;
using Assets.Scripts.Models;
using Assets.Scripts.Menus.Overlays;

public abstract class BaseContent : MonoBehaviour, IContentOverlay
{
    protected MoveUIScreenOverlay moveOverlay;
    protected bool activate;
    protected float transitionTime;

    private bool StartAtOrigin;

    protected Color baseColor;
    protected string baseResourceName;
    protected bool canPerformActions;

    //MonoBehaviour
    private void Awake()
    {
        moveOverlay = GetComponent<MoveUIScreenOverlay>();

        AwakeMono();
    }

    private void Start()
    {
        StartMono();
    }

    //virtuals
    protected virtual void AwakeMono() { }
    protected virtual void StartMono() { }
    protected virtual void UpdateContent(BaseContentOverlay contentOverlay) { }
    protected virtual void ToggleContent()
    {
        if (activate)
            activate = false;
        else
            StartCoroutine(TillDestroy());

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.ShowInstructions);
    }

    //Implementations
    public void UpdateModel(BaseContentOverlay contentOverlay)
    {
        moveOverlay.SetPositions(contentOverlay);

        activate = true;
        StartAtOrigin = contentOverlay.StartAtOrigin;
        transitionTime = contentOverlay.TransitionTime;

        baseResourceName = contentOverlay.BaseResourceName;

        UpdateContent(contentOverlay);
    }

    public void Toggle()
    {
        if (StartAtOrigin)
            StartAtOrigin = false;
        else
            moveOverlay.Toggle(transitionTime);

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.HideInstructions);

        ToggleContent();
    }

    public bool CanPerformActions()
    {
        return canPerformActions;
    }

    //Pseudo-Events
    public virtual void OnClickBackOrXButton()
    {
        Toggle();
    }

    //Abstractions
    protected IEnumerator TillDestroy()
    {
        yield return new WaitForSecondsRealtime(transitionTime + transitionTime);

        //Just to be safe..
        StopAllCoroutines();

        Destroy(transform.gameObject);
    }

    //General Methods
    protected void SetBaseColor(Color color)
    {
        baseColor = color;
    }

    protected void SetBaseColor(BaseContentOverlay content)
    {
        if(content is BaseColor)
            baseColor = ((BaseColor)content).GetColor();
    }
}
