﻿namespace Assets.Scripts.Menus.Overlays
{
    public interface IContentOverlay
    {
        void UpdateModel(Models.BaseContentOverlay contentOverlay);

        void Toggle();

        //This is Fruit Behaviors Or showing Instructions
        bool CanPerformActions();
    }
}
