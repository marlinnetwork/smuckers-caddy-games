﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using Lean.Touch;

	public class EraseGesture : LeanFingerTrail
	{
		[Tooltip("Angle required to trigger a new line.")]
        [SerializeField]
        public float NewLineAngleDeg = 30.0f;
        private float NewLineAngleRad = 0.0f;

        [Range(1,5)]
		[Tooltip("Number of lines needed for a total erasure.")]
        public int EraseLinesNeeded = 4;
        private int numEraseLines = 0;
        private float lastAngleOfApproachRad = 0.0f;
        private float lastAngleOfApproachDeg = 0.0f;
        private Vector2 lastWorldPoint;


        protected override void Start() {

            lastWorldPoint = Vector2.zero;
            NewLineAngleRad = NewLineAngleDeg * Mathf.Deg2Rad;
            
            var UICameraObj = GameObject.Find("UI Camera");

            if(UICameraObj)
            {
                ScreenDepth.Camera = UICameraObj.GetComponent<Camera>();
                Debug.Log("Setting erase gesture camera to UI camerea.");
            }

            base.Start();
        }

		protected override void WritePositions(LineRenderer line, LeanFinger finger)
        {
			line.positionCount = finger.Snapshots.Count;
            Vector2 newWorldPoint = Vector2.zero;

			// Loop through all snapshots
			for (var i = 0; i < finger.Snapshots.Count; i++)
			{
				var snapshot = finger.Snapshots[i];

				// Get the world postion of this snapshot
				newWorldPoint = ScreenDepth.Convert(snapshot.ScreenPosition, gameObject);

				// Write position
				line.SetPosition(i, newWorldPoint);
			}


            var angleOfApproach = Mathf.Acos(Vector2.Dot(lastWorldPoint, newWorldPoint));
            var angleOfApproachDeg = angleOfApproach * Mathf.Rad2Deg;

            lastWorldPoint = newWorldPoint;

            // Debug.Log("Current Angle: " + angleOfApproachDeg + " Last Angle: " + lastAngleOfApproachDeg);

            if((Mathf.Abs(angleOfApproachDeg - lastAngleOfApproachDeg)) >= NewLineAngleDeg)
            {
                numEraseLines += 1;
                lastAngleOfApproachRad = angleOfApproach;
                lastAngleOfApproachDeg = lastAngleOfApproachRad * Mathf.Rad2Deg;

                if(numEraseLines > EraseLinesNeeded)
                {
                    numEraseLines = 0;
                    Debug.Log("Play our final animation.");
                }
            }
            
        }

	}
