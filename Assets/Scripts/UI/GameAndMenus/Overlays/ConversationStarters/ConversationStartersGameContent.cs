﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;

public class ConversationStartersGameContent : BaseContent
{
    [SerializeField]
    public Sprite[] Splats;

    [SerializeField]
    public Image Splat;

    [SerializeField]
    public int MinSplatScale;

    [SerializeField]
    public int MaxSplatScale;

    [SerializeField]
    public Text Category;

    [SerializeField]
    public Text ConversationStarter;

    protected override void AwakeMono()
    {
        RectTransform splatRectTransfom = Splat.GetComponent<RectTransform>();

        splatRectTransfom.Rotate(new Vector3(0, 0, Random.Range(0, 360)));
    }

    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        ConversationStartersGameModel model = (ConversationStartersGameModel)contentOverlay;
        
        Splat.sprite = Splats[Random.Range(0, (Splats.Length))];
        Splat.color = model.SplatColor;
        Splat.transform.localScale = new Vector3(Random.Range(MinSplatScale, MaxSplatScale), Random.Range(MinSplatScale, MaxSplatScale), 1);

        Category.color = model.SplatColor;

        SetBaseColor(contentOverlay);

        LoadModel();
    }

    protected override void ToggleContent()
    {
        if(activate)
        {
            //anything that needs to aminate or function here
            activate = false;
        }
        else
        {
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new GearMenuModel(baseColor, baseResourceName));

            StartCoroutine(TillDestroy());
        }
    }

    private void LoadModel()
    {
        ConversationStarterModel model = Messages.GamesControllerBus.GetNextConversationStarter();

        Category.text = model.Category;
        ConversationStarter.text = model.ConversationStarter;
    }
}