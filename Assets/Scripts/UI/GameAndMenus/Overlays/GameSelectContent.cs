﻿using Assets.Scripts.Models;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class GameSelectContent : BaseContent
{
    public void OnClickConversationStarters()
    {
        LoadSplash(new ConversationStartersSplashModel());
    }

    public void OnClickFunFacts()
    {
        LoadSplash(new FunFactsSplashModel());
    }

    public void OnClickFruitSmash()
    {
        LoadSplash(new FruitSmashSplashModel());
    }

    private void LoadSplash(BaseColor model)
    {
        Analytics.CustomEvent("click", new Dictionary<string, object>() {
            { "element", "Main Options Menu" },
            { "game" , model.BaseResourceName }
        });

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, model);

        Toggle();
    }

    public override void OnClickBackOrXButton()
    {
        Messages.AppControllerBus.SetActivePCState(PCControllerState.Selectable);

        Toggle();
    }
}
