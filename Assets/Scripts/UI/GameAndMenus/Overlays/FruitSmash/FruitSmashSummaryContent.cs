﻿using UnityEngine;
using Assets.Scripts.Models;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class FruitSmashSummaryContent : BaseContent
{
    [SerializeField]
    public Text summaryText;

    protected override void AwakeMono()
    {
        Messages.GamesControllerBus.SetActiveGameMode(GameMode.Completed);

        AnalyticsEvent.GameStart(new Dictionary<string, object>() {
            {"game_start", baseResourceName}
        });
    }

    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        FruitSmashSummaryModel content = (FruitSmashSummaryModel)contentOverlay;

        summaryText.text = summaryText.text.Replace("{{gameScore}}", content.Score.ToString())
                            .Replace("{{gameTimeSpan}}", content.Time.ToString());
    }

    public void OnClickYes()
    {
        Analytics.CustomEvent("click", new Dictionary<string, object>() {
            { "element", "Play Again" },
            { "game", Messages.GamesControllerBus.GetActiveGame() }
        });

        LoadNextOverlay(new FruitSmashSplashModel());
    }

    public void OnClickNo()
    {
        LoadNextOverlay(new GameSelectModel());
    }

    private void LoadNextOverlay(BaseContentOverlay contentOverlay)
    {
        Messages.GamesControllerBus.SetCurrentActiveGame(string.Empty);
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, contentOverlay);

        Toggle();
    }
}
