﻿using Assets.Scripts.Models;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class FruitSmashEndContent : EndContent
{
    private FruitSmashSettingsModel model;

    protected override void StartMono()
    {
        model = (FruitSmashSettingsModel)Messages.GamesControllerBus.GetActiveGameSettings();
    }
    
    public void OnClickChangeGamePreferences()
    {
        if(model == null)
            model = (FruitSmashSettingsModel)Messages.GamesControllerBus.GetActiveGameSettings();

        Analytics.CustomEvent("click", new Dictionary<string, object>() {
            { "element", "Change Game Preferences" },
            { "game", Messages.GamesControllerBus.GetActiveGame() }
        });

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, model);

        Toggle();
    }

    public override void OnClickBackOrXButton()
    {
        FruitSmashGameModel model = new FruitSmashGameModel()
        {
            GameMode = GameMode.Paused
        };

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Games, GameMenuOverlay.Load, model);

        Toggle();
    }

    public override void OnEndGame()
    {
        Messages.GamesControllerBus.ResetActiveGameState();

        base.OnEndGame();
    }
}