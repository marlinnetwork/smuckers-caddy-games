﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Collections.Generic;
using Assets.Scripts.Models;

public class FruitSmashSettingsContent : SettingsContent
{
    [SerializeField]
    public Slider slider;

    [SerializeField]
    public Text displayTime;
    
    private int GameTime;

    private const int minFontSize = 145;
    private const int maxFontSize = 200;
    private const int incrementor = 5;

    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        GameTime = ((FruitSmashSettingsModel)contentOverlay).Time;

        slider.value = (GameTime / 10);

        OnValueChanged(slider.value);
    }

    public override void OnClickContinue()
    {
        FruitSmashSettingsModel model = new FruitSmashSettingsModel()
        {
            Time = GameTime,
            Score = 0
        };

        Analytics.CustomEvent("click", new Dictionary<string, object>() {
            { "element", "Time Selection"} ,
            { "time", GameTime },
            { "game", Messages.GamesControllerBus.GetActiveGame() }
        });

        Messages.GamesControllerBus.SetActiveGameSettings(model);

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new FruitSmashSplashModel());

        base.OnClickContinue();
    }

    public void OnValueChanged(float value)
    {
        GameTime = (int)(value * 10);

        displayTime.text = GameTime.ToString();
    }

    public override void OnClickBackOrXButton()
    {
        FruitSmashGameModel model = new FruitSmashGameModel()
        {
            GameMode = GameMode.Paused
        };

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Games, GameMenuOverlay.Load, model);

        Toggle();
    }
}