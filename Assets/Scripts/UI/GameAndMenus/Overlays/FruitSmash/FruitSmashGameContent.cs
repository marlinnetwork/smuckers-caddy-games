﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using Assets.Scripts.Models;
using System;

public class FruitSmashGameContent : BaseContent
{
    [SerializeField]
    public Image ProgressBar;

    [SerializeField]
    public Text TimeLeft;

    [SerializeField]
    public Image GearBackground;

    [Space(5)]
    [SerializeField]
    public Sprite[] AvailableFruitImages;

    [SerializeField]
    public Image FruitImage;

    [SerializeField]
    public Text Popped;

    private float overlayTransitionTime;
    private int currentTime;
    private int startTime;

    private int getScore
    {
        get
        {
            int score = 0;

            Int32.TryParse(Popped.text, out score);

            return score;
        }
    }

    protected override void AwakeMono()
    {
        canPerformActions = true;

        Messages.GamesControllerBus.SetActiveGameMode(GameMode.Playing);
    }

    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        SetBaseColor(contentOverlay);

        GearBackground.color = baseColor;

        FruitSmashGameModel model = (FruitSmashGameModel)contentOverlay;

        FruitImage.sprite = AvailableFruitImages.FirstOrDefault(FOD => FOD.name == model.FruitType.ToString());
        overlayTransitionTime = model.TransitionTime;
        startTime = model.StartTime;

        SetCurrentState(model);

        StartCoroutine(StartCountDown());
    }

    //This will be considered the only update for the class
    private void Update()
    {
        //This may be heavy, but seems to be how this chould be
        SetCurrentState((FruitSmashGameModel)Messages.GamesControllerBus.GetActiveGameState());
    }

    protected override void ToggleContent()
    {
        if (activate)
        {
            activate = false;
        }
        else
        {
            StartCoroutine(TillDestroy());
        }
    }

    public void LoadEndContent()
    {
        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, new FruitSmashEndModel());

        Toggle();
    }

    private void SetCurrentState(FruitSmashGameModel model)
    {
        if(model != null)
        {
            Popped.text = model.Score.ToString();
            TimeLeft.text = model.Time.ToString();
            currentTime = model.Time;
            ProgressBar.fillAmount = ((float)currentTime / (float)startTime);
        }
    }

    private IEnumerator StartCountDown()
    {
        // yield return new WaitForSeconds(overlayTransitionTime);
        yield return new WaitForEndOfFrame();

        StartCoroutine(DecrementCountDown());

        StopCoroutine(StartCountDown());
    }

    private IEnumerator DecrementCountDown()
    {
        yield return new WaitForSecondsRealtime(1f);

        Messages.GamesControllerBus.UpdateActiveGameTime(-1);

        //Since we can get to a state where -1 is time, we dont ever want to show it.
        if (currentTime > 1)
            StartCoroutine(DecrementCountDown());
        else
        {
            StopCoroutine(DecrementCountDown());

            FruitSmashSummaryModel model = new FruitSmashSummaryModel()
            {
                Time = startTime,
                Score = getScore,
            };

            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, model);

            Toggle();
        }
    }
}