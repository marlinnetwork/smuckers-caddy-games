﻿using Assets.Scripts.Models;

public class FruitSmashSplashContent : SplashContent
{
    protected override void ToggleContent()
    {
        if (activate)
        {
            StartCoroutine(StartCountDown());

            activate = false;
        }
        else
        {
            //Set the Active game. we have to set this here because we override splashcontent
            Messages.GamesControllerBus.SetCurrentActiveGame(baseResourceName);
            
            //If we are running the splash screen its either a setting change or a restart of the game.
            Messages.GamesControllerBus.ResetActiveGameState();

            //Load the game
            FruitSmashGameModel model = (FruitSmashGameModel)Messages.GamesControllerBus.GetActiveGameState();

            model.FruitType = Messages.AppControllerBus.GetActivePCFruitType();

            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Games, GameMenuOverlay.Load, model);

            //Load the instructions.
            Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Instructions, UIInstructions.SetBackgroundCopy, GetUIInstructions());

            StartCoroutine(TillDestroy());
        }
    }
}
