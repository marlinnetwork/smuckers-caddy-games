﻿using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;


public class GearMenuContent : BaseContent
{
    private string NextResourceName { get; set; }

    [SerializeField]
    public Image image;

    protected override void AwakeMono()
    {
        canPerformActions = true;
    }

    protected override void UpdateContent(BaseContentOverlay contentOverlay)
    {
        NextResourceName = ((GearMenuModel)contentOverlay).NextResourceName ;

        SetBaseColor(contentOverlay);

        image.color = baseColor;
    }

    //public override string GetAnalyticsName()
    //{
    //    return "";
    //}

    public override void OnClickBackOrXButton()
    {
        base.OnClickBackOrXButton();
    }

    public void LoadEndContent()
    {
        Toggle();

        Messages.UIOverlayControllerBus.SendMessage(UIMessageFor.Menus, GameMenuOverlay.Load, GetEndContent());
    }

    private BaseContentOverlay GetEndContent()
    {
        switch(NextResourceName)
        {
            case "conversationstarters":
                return new ConversationStartersEndModel();
            case "funfacts":
                return new FunFactsEndModel();
            case "fruitsmash":
                return new FruitSmashEndModel();
            default:
                return null;
        }
    }
}
