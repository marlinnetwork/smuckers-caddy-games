﻿using System.Collections;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Menus.Overlays;

public class InstructionsController : MonoBehaviour
{
    [SerializeField]
    public int ShowInstructionsTime;
    [SerializeField]
    public int HideInstructionsTime;

    [HideInInspector]
    public string GetBackgroundTextValue { get { return background.GetTextValue; } }
    public string GetCurrentTime { get { return currentTime.ToString(); } }
    public string GetCurrentBackgroundState { get { return background.gameObject.activeSelf.ToString(); } }

    private int currentTime;

    private BackgroundController background;

    private void Awake()
    {
        background = GetComponentInChildren<BackgroundController>();

        Reset();

        StartCoroutine(TillShowInstructions());
    }

    public void Message(UIInstructions type, object obj)
    {
        switch (type)
        {
            case UIInstructions.SetBackgroundCopy:
                SetBackgroundCopy((string)obj);
                break;
            case UIInstructions.ResetCurrentTimer:
                ResetCurrentTimer();
                break;
            case UIInstructions.ShowInstructions:
                ShowInstruction();
                break;
            case UIInstructions.HideInstructions:
                HideInstructions();
                break;
        }
    }

    private void SetBackgroundCopy(string copy)
    {
        background.ToggleState(false);
        background.SetCopy(copy);
        ResetCurrentTimer();
    }

    private void ResetCurrentTimer()
    {
        currentTime = 0;
    }

    private void Reset()
    {
        background.ToggleState(false);
        ResetCurrentTimer();
    }

    private void ShowInstruction()
    {
        currentTime = ShowInstructionsTime;

        StopAllCoroutines();
        StartCoroutine(TillShowInstructions());
    }

    private void HideInstructions()
    {
        currentTime = 0;

        if (background != null)
            background.ToggleState(false);

        StopAllCoroutines();
        StartCoroutine(TillShowInstructions());
    }

    IEnumerator TillShowInstructions()
    {
        yield return new WaitForSecondsRealtime(1f);

        if (currentTime <= ShowInstructionsTime)
        {
            currentTime++;

            StartCoroutine(TillShowInstructions());
        }
        else
        {
            if (background != null && UIOverlayController.ValidateOverlayCanShowInstructions())
                background.ToggleState(true);

            currentTime = HideInstructionsTime;

            StopCoroutine(TillShowInstructions());
            StartCoroutine(TillHideInstructions());
        }
    }

    IEnumerator TillHideInstructions()
    {
        yield return new WaitForSecondsRealtime(1f);

        if (currentTime > 0)
        {
            currentTime--;

            StartCoroutine(TillHideInstructions());
        }
        else
        {
            if(background != null)
                background.ToggleState(false);

            StopCoroutine(TillHideInstructions());
            StartCoroutine(TillShowInstructions());
        }
    }
}
