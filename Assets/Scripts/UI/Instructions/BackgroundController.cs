﻿using UnityEngine;
using UnityEngine.UI;

public class BackgroundController : MonoBehaviour
{
    private Text text;

    [HideInInspector]
    public string GetTextValue {get { return text.text; }}

    public void ToggleState(bool state)
    {
        if(gameObject != null)
            gameObject.SetActive(state);
    }

    public void SetCopy(string copy)
    {
        if(text == null)
            text = GetComponentInChildren<Text>();

        text.text = copy;
    }
}