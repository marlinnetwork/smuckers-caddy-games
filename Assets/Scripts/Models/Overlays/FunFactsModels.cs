﻿namespace Assets.Scripts.Models
{
    public class FunFactsSplashModel : BaseFunFactsColor
    {
        public FunFactsSplashModel() :base()
        {
            ResourceName = "funfacts_splash";
            OutLocation = RelativeLocation.Left;
        }
    }

    public class FunFactsEndModel : BaseFunFactsColor
    {
        public FunFactsEndModel() : base()
        {
            ResourceName = "funfacts_end";
            StartAtOrigin = true;
            OutLocation = RelativeLocation.Right;
        }
    }

    public class FunFactsSettingsModel : BaseFunFactsColor
    {
        public string[] DisabledFunFacts { get; set; }

        public FunFactsSettingsModel() : base()
        {
            ResourceName = "funfacts_settings";
            OutLocation = RelativeLocation.Right;
        }
    }

    public class FunFactsGameModel : BaseFunFactsColor
    {
        public string Category;

        public FunFactsGameModel() : base()
        {
            ResourceName = "funfacts_game";
            OutLocation = RelativeLocation.Bottom;
        }
    }
}