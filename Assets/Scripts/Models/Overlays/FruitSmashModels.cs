﻿namespace Assets.Scripts.Models
{
    public class FruitSmashSplashModel : BaseFruitSmashColor
    {
        public FruitSmashSplashModel() : base()
        {
            ResourceName = "fruitsmash_splash";
            OutLocation = RelativeLocation.Left;
        }
    }

    public class FruitSmashEndModel : BaseFruitSmashColor
    {
        public FruitSmashEndModel() : base()
        {
            ResourceName = "fruitsmash_end";
            StartAtOrigin = true;
            OutLocation = RelativeLocation.Left;
        }
    }

    public class FruitSmashSettingsModel : BaseFruitSmashColor
    {
        public int Time { get; set; }
        public int Score { get; set; }

        public FruitSmashSettingsModel() : base()
        {
            ResourceName = "fruitsmash_settings";
            OutLocation = RelativeLocation.Right;
            Time = 30;
            Score = 0;
        }
    }

    public class FruitSmashSummaryModel : FruitSmashSettingsModel
    {
        public FruitSmashSummaryModel() : base()
        {
            ResourceName = "fruitsmash_summary";
            OutLocation = RelativeLocation.Right;
        }
    }

    public class FruitSmashGameModel : FruitSmashSettingsModel
    {
        public GameMode GameMode;
        public FruitType FruitType;
        public int StartTime;

        public FruitSmashGameModel () : base()
        {
            ResourceName = "fruitsmash_game";
            OutLocation = RelativeLocation.Top;
            GameMode = GameMode.Created;
        }
    }
}
