﻿namespace Assets.Scripts.Models
{
    public abstract class BaseContentOverlay
    {
        public string ResourceName;

        public RelativeLocation OutLocation;
        public bool StartAtOrigin = false;
        public float TransitionTime = .5f;
        public bool UseBumpPoints = true;

        public string BaseResourceName
        {
            get { return ResourceName.Split('_')[0]; }
        }
    }

    public abstract class BaseColor : BaseContentOverlay
    {
        protected UnityEngine.Color Color = UnityEngine.Color.red;

        public UnityEngine.Color GetColor() { return Color;  }

        public BaseColor() : base() { }
        public BaseColor(UnityEngine.Color color) : base()
        {
            Color = color;
        }
    }

    public class GameSelectModel : BaseContentOverlay
    {
        public GameSelectModel() : base()
        {
            ResourceName = "gameselect";
            StartAtOrigin = true;
            OutLocation = RelativeLocation.Left;
        }
    }

    public class GearMenuModel : BaseColor
    {
        public string NextResourceName { get; set; }

        public GearMenuModel(UnityEngine.Color color, string nextResourceName) : base(color)
        {
            ResourceName = "gearmenu";
            OutLocation = RelativeLocation.TopRight;
            TransitionTime = .75f;
            NextResourceName = nextResourceName;
            UseBumpPoints = false;
        }
    }

    public abstract class BaseConversationStartersColor : BaseColor
    {
        public BaseConversationStartersColor() : base(new UnityEngine.Color(.42f, .11f, .78f, 1f)) { }
    }

    public abstract class BaseFunFactsColor : BaseColor
    {
        public BaseFunFactsColor() : base(new UnityEngine.Color(.27f, .31f, .6f, 1f)) { }
    }

    public abstract class BaseFruitSmashColor : BaseColor
    {
        public BaseFruitSmashColor() : base(new UnityEngine.Color(.26f, .46f, .07f, 1f)) { }
    }
}