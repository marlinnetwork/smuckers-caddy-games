﻿namespace Assets.Scripts.Models
{
    public class ConversationStartersSplashModel : BaseConversationStartersColor
    {
        public ConversationStartersSplashModel() : base()
        {
            ResourceName = "conversationstarters_splash";
            OutLocation = RelativeLocation.Left;
        }
    }

    public class ConversationStartersEndModel : BaseConversationStartersColor
    {
        public ConversationStartersEndModel() : base()
        {
            ResourceName = "conversationstarters_end";
            StartAtOrigin = true;
            OutLocation = RelativeLocation.Bottom;
        }
    }

    public class ConversationStartersGameModel : BaseConversationStartersColor
    {
        public UnityEngine.Color SplatColor = UnityEngine.Color.red;

        public ConversationStartersGameModel() : base()
        {
            ResourceName = "conversationstarters_game";
            StartAtOrigin = true;
            OutLocation = RelativeLocation.Bottom;
        }
    }
}
