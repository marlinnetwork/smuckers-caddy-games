﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class BackgroundModel : IEquatable<BackgroundModel>
    {
        public UnityEngine.Color color { get; set; }

        public CopyAndBackgroundType[] ApplyTo { get; set; }

        public bool Equals(BackgroundModel other)
        {
            return other != null && 
                    color.Equals(other.color);
        }

        public override int GetHashCode()
        {
            return 790427672 + EqualityComparer<UnityEngine.Color>.Default.GetHashCode(color);
        }
    }

    [Serializable]
    public class CopyAndBackgroundModel : BackgroundModel, IEquatable<CopyAndBackgroundModel>
    {
        public string pcName { get; set; }

        public string fruitType { get; set; }

        public string productType { get; set; }

        public UnityEngine.Color? copyColor { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as CopyAndBackgroundModel);
        }

        public bool Equals(CopyAndBackgroundModel other)
        {
            return other != null &&
                        pcName == other.pcName &&
                        EqualityComparer<UnityEngine.Color?>.Default.Equals(copyColor, other.copyColor);
        }

        public override int GetHashCode()
        {
            var hashCode = 1524571445;

            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(pcName);
            hashCode = hashCode * -1521134295 + EqualityComparer<UnityEngine.Color?>.Default.GetHashCode(copyColor);

            return hashCode;
        }
    }

    public class CopyAndBackgroudModelDTO
    {
        public UnityEngine.Color color { get; set; }
        public String copy { get; set; }
        public UnityEngine.Color? copyColor { get; set; }
    }
}
