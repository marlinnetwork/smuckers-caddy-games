﻿using UnityEngine;

namespace Assets.Scripts.Models
{
    public abstract class BaseEffectModel
    {
        //So this is the base of any effect.
        // we can add more as we go
        // surrogate particale system
        [Header("Overridden if a color is pass to the Effect")]
        public Color Color = Color.white;

        [Space(5)]
        [Range(0f, 10f)]
        public float Duration = 1f;

        [Space(5)]
        [Range(0f, 1f)]
        public float AlphaStart = 1f;

        [Range(0f, 1f)]
        public float AlphaTo = 1f;

        [Space(5)]
        [Range(-3f, 3f)]
        public float ScaleFactor = 1f;
    }

    public abstract class BaseFruitEffect : BaseEffectModel
    {
        //[SerializeField]
        //public bool EffectFacesMainCamera;
    }

    [System.Serializable]
    public class PulseModel : BaseEffectModel
    {
        [SerializeField]
        public bool ScaleToParent = true;
    }

    [System.Serializable]
    public class SplatModel : BaseEffectModel
    {
        [SerializeField]
        [Range(-3f, 3f)]
        public float ScaleStart = 1f;

        [SerializeField]
        public Material[] Splats;
    }

    [System.Serializable]
    public class ExplodeModel : BaseFruitEffect
    {
        [SerializeField]
        public bool ScaleToParent = true;
    }

    [System.Serializable]
    public class UndulateModel : BaseFruitEffect
    {
        [Space(5)]
        [Header("Runs effect to final state and back to original state")]
        [SerializeField]
        public bool PingPong;

        [HideInInspector]
        public string Category;
    }
}
