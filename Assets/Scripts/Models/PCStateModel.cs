﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class PCStateModel
    {
        public PCControllerState ControllerState { get; set; }

        public string ActiveGame { get; set; }

        public PCStateModel ()
        {
            ControllerState = PCControllerState.InActivate;
            ActiveGame = string.Empty;
        }

        public PCStateModel(PCControllerState state, string activeGame)
        {
            ControllerState = state;
            ActiveGame = activeGame;
        }
    }
}
