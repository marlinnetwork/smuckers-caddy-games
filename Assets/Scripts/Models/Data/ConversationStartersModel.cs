﻿namespace Assets.Scripts.Models
{
    [System.Serializable]
    public class ConversationStarters
    {
        public ConversationStarterModel[] results;
    }

    [System.Serializable]
    public class ConversationStarterModel : BaseDataModel
    {
        public string Category;
        public string ConversationStarter;
    }
}