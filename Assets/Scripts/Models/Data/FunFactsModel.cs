﻿namespace Assets.Scripts.Models
{
    [System.Serializable]
    public class FunFacts
    {
        public FunFactModel[] results;
    }

    [System.Serializable]
    public class FunFactModel : BaseDataModel
    {
        public string Category;
        public string FunFact;
        public string Answer;
        public string URL;
    }
}