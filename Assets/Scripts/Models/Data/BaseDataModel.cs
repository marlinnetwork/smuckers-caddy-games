﻿namespace Assets.Scripts.Models
{
    /// <summary>
    /// This is for The BaseCategoryController. 
    /// To share abstract functionality across 2 games
    /// This may seems to be overkill, but there may be more games like this later on
    /// using lists of data
    /// </summary>
    public abstract class BaseDataModel
    {
        //So this is in response to an analytics request. Abstract for analytics may be pending..
        public bool Counted { get; set; }
    }
}
