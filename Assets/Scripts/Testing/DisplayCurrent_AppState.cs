﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayCurrent_AppState : MonoBehaviour {

    public Text ActivePC;
    public Text ActivePCPlayingGame;
    public Text ActiveGame;
    public Text ActivePCQueue;
    
    private void Update()
    {
        ActivePC.text = AppController.GetCurrentAppController().GetActivePCName;
        Assets.Scripts.Models.PCStateModel state = AppController.GetCurrentActivePC()?.GetControllersCurrentStateModel();

        if (state != null)
        {
            ActivePCPlayingGame.text = "state: " + state.ControllerState.ToString();
            ActiveGame.text = "game: " + state.ActiveGame;
        }

        ActivePCQueue.text = AppController.GetCurrentAppController().GetActivePCQueue;
    }
}
