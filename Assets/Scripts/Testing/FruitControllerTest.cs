﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

class FruitControllerTest : FruitsController
{
    private ResourceService service;

    public ToggleGroup toggleGroup;

    private void Start()
    {
        service = GetComponent<ResourceService>();
    }

    public void spawnFruit()
    {
        Toggle toggle = toggleGroup.ActiveToggles().FirstOrDefault();

        FruitType selected = (toggle.name == "Grape") ? FruitType.Grape : FruitType.Strawberry;

        UpdateSpawnableFruit(service.GetFruitByFruitType(selected), Color.red);
        UpdateState(new Assets.Scripts.Models.PCStateModel(PCControllerState.Selectable, string.Empty));

        SpawnFruits();
    }
}