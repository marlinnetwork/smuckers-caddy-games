﻿
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Models;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Testing
{
    public partial class PCController_StateTest : MonoBehaviour
    {
        public PCController _pcController;
        public ToggleGroup toggleGroup;

        //This is set based on the ProdutionType on the controller it self. Testing should be set on the state of the PCController
        //private bool IsLaunchAnyway;

        //These are the states for the background and copy
        private bool IsBackgroundOnly = false;
        private bool IsAll = true;

        public void ToggleIsVisible() { _pcController.ToggleIsVisible(); }                  //Is this PC Currently on the screen
        public void ToggleIsPlayingGame() { _pcController.ToggleIsPlayingGame(); }          //tapped and a game is running
        public void ToggleIsActive() { _pcController.ToggleIsActive(); }                    //ready to tap
        public void ToggleIsTooManyPCs() { _pcController.ToggleIsTooManyPCs(); }            //multiple pc visible

        public void ToggleIsBackgroundOnly () { IsBackgroundOnly = !IsBackgroundOnly; }
        public void ToggleIsAll () { IsAll = !IsAll; }

        public void Awake()
        {
            ToggleIsVisible();

            toggleGroup.ActiveToggles().ToList().ForEach(FE =>
            {
                switch(FE.name)
                {
                    case "IsActive":
                        ToggleIsActive();
                        break;
                    case "ToggleIsTooManyPCs":
                        ToggleIsTooManyPCs();
                        break;
                    case "IsPlayingGame":
                        ToggleIsPlayingGame();
                        break;
                }
            });
        }

        public void PCControllerStateTest()
        {
            CopyAndBackgroundModel model = IsBackgroundOnly ? 
                SetBackgroundsColor() :
                SetCopyAndBackgroundColor();
            
            _pcController.SendPCContollerModel(model);
            _pcController.TEST_UpdateState();
        }
      
        private CopyAndBackgroundModel SetCopyAndBackgroundColor()
        {
            var i = new CopyAndBackgroundModel()
            {
                color = GetRandColor(),
                pcName = GetRandCopy(),
                fruitType = GetRandCopy(),
                copyColor = GetRandColor(),
                ApplyTo = GetTypes(IsAll)
            };
            return i;
        }

        private CopyAndBackgroundModel SetBackgroundsColor()
        {
            var i = new CopyAndBackgroundModel()
            {
                color = GetRandColor(),
                ApplyTo = GetTypes(IsAll)
            };
            return i;
        }

        private Color GetRandColor()
        {
            return new Color(Random.value, Random.value, Random.value);
        }
        
        private string GetRandCopy()
        {
            //These are just fruit types from PCController.
            //This functionality will be more robust based on pc choices, but for now
            // just want to test.
            System.Array values = System.Enum.GetValues(typeof(FruitType));

            return values.GetValue(Random.Range(0, values.Length)).ToString(); ;
        }

        private CopyAndBackgroundType[] GetTypes(bool isAll)
        {
            IList<CopyAndBackgroundType> list = new List<CopyAndBackgroundType>();
            var values = System.Enum.GetValues(typeof(CopyAndBackgroundType));

            if(IsAll)
            {
                foreach(CopyAndBackgroundType val in values)
                {
                    list.Add(val);
                }
            }
            else
                list.Add((CopyAndBackgroundType)values.GetValue(Random.Range(0, values.Length)));

            return list.ToArray();
        }
    }
}