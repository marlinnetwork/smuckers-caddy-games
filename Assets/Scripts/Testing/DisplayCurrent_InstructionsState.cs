﻿using UnityEngine;
using UnityEngine.UI;


public class DisplayCurrent_InstructionsState : MonoBehaviour
{
    [SerializeField]
    public Text CurrentTime;

    [SerializeField]
    public Text BackgroundState;

    [SerializeField]
    public Text CurrentInstructions;

    private void Update()
    {
        CurrentTime.text = UIOverlayController.GetCurrentUIOverlayController().GetCurrentTime;
        BackgroundState.text = UIOverlayController.GetCurrentUIOverlayController().GetCurrentBackgroundState;
        CurrentInstructions.text = UIOverlayController.GetCurrentUIOverlayController().GetInstructionTextValue;
    }
}
