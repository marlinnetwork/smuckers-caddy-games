﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

namespace Assets.Scripts.Testing
{
    public partial class PCController_AppTest : MonoBehaviour
    {
        public PCController _pcController1;
        public PCController _pcController2;
        public ToggleGroup toggleGroup;

        public Text ActivePC;
        public Text ActivePCQueue;

        private void Update()
        {
            ActivePC.text = AppController.GetCurrentAppController().GetActivePCName;
            ActivePCQueue.text = AppController.GetCurrentAppController().GetActivePCQueue;
        }

        public void TEST_AppControl()
        {
            const float fast = .1f;
            const float med = .5f;
            const float slow = 2f;

            //get the active toggle
            Toggle active = toggleGroup.ActiveToggles().FirstOrDefault();

            //sets time.
            switch(active.name)
            {
                case "PC1Single":
                    StartCoroutine(SendFound(_pcController1, fast));
                    break;
                case "PC2Single":
                    StartCoroutine(SendFound(_pcController2, fast));
                    break;
                case "PC1Pre-Active":
                    StartCoroutine(SendFound(_pcController1, med));
                    StartCoroutine(SendFound(_pcController2, fast));
                    break;
                case "PC2Pre-Active":
                    StartCoroutine(SendFound(_pcController1, fast));
                    StartCoroutine(SendFound(_pcController2, med));
                    break;
                case "PC1Post-Active":
                    StartCoroutine(SendFound(_pcController1, slow));
                    StartCoroutine(SendFound(_pcController2, med));
                    break;
                case "PC2Post-Active":
                    StartCoroutine(SendFound(_pcController1, med));
                    StartCoroutine(SendFound(_pcController2, slow));
                    break;
                case "PC1PC2sametime":
                    StartCoroutine(SendFound(_pcController1, fast));
                    StartCoroutine(SendFound(_pcController2, fast));
                    break;
                case "RemoveActive":
                    AppController.GetCurrentActivePC().Lost();
                    break;
                case "RemoveFirstInactive":
                    PCController activePC = AppController.GetCurrentActivePC();
                    AppController.GetCurrentActivePCQueue().FirstOrDefault(FOD => FOD != activePC).Lost();
                    break;
            }

            //This was fine removing what was, but the above now is more realistic.
            //if (pc1Wait != 0f)
            //    StartCoroutine(SendFound(_pcController1, pc1Wait));
            //else
            //    _pcController1.Lost();

            //if (pc2Wait != 0f)
            //    StartCoroutine(SendFound(_pcController2, pc2Wait));
            //else
            //    _pcController2.Lost();

        }

        public void Test_Reset()
        {
            AppController.GetCurrentActivePCQueue().ForEach(FE => FE.Lost());
        }

        IEnumerator SendFound(PCController controller, float wait)
        {
            yield return new WaitForSecondsRealtime(wait);

            controller.Found();
        }
    }
}