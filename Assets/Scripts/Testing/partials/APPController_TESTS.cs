﻿using UnityEngine;
using System.Linq;

/*
 * Any needed test functionality can be added here insted of the directly 
 * on the pc controller. Just some separation
 */

public sealed partial class AppController : MonoBehaviour
{
    public string GetActivePCName
    {
        get {
            return (ActivePC != null) ?
                ActivePC.name :
                "No Active"; }
    }

    public string GetActivePCQueue
    {
        get {
            return (ActivePCQueue.Count > 0) ?
                string.Join(", ", ActivePCQueue.Select(S => S.name).ToArray()) :
                "Queue Empty";
        }
    }
}
