﻿using UnityEngine;
using Assets.Scripts.Models;

/*
 * Any needed test functionality can be added here insted of the directly 
 * on the pc controller. Just some separation
 */

public partial class PCController : MonoBehaviour
{
    public void ToggleIsVisible() { /*_pcState.Internal[PCStateCreator.PCProperty.Visible] = !_pcState.Internal[PCStateCreator.PCProperty.Visible]; */}
    public void ToggleIsPlayingGame() { /*_pcState.Internal[PCStateCreator.PCProperty.PlayingGame] = !_pcState.Internal[PCStateCreator.PCProperty.PlayingGame];*/ }
    public void ToggleIsActive() { /*_pcState.Internal[PCStateCreator.PCProperty.Active] = !_pcState.Internal[PCStateCreator.PCProperty.Active];*/ }
    public void ToggleIsTooManyPCs() { /*_pcState.Internal[PCStateCreator.PCProperty.TooManyPCs] = !_pcState.Internal[PCStateCreator.PCProperty.TooManyPCs];*/ }

    public void SendPCContollerModel(CopyAndBackgroundModel model)
    {
        //Messageing State
        //_pcMessagingController.UpdateModel(model);
        //if we have a model state we can pass it here
        //_pcActivationController.
    }

    public void TEST_UpdateState()
    {
        ReceiveNewState(PCControllerState.Selectable);
    }
}