﻿using System;
using UnityEngine;

public sealed partial class UIOverlayController : MonoBehaviour
{
    [HideInInspector]
    public string GetInstructionTextValue { get { return _instructionsController.GetBackgroundTextValue; } }
    [HideInInspector]
    public string GetCurrentTime { get { return _instructionsController.GetCurrentTime; } }
    [HideInInspector]
    public string GetCurrentBackgroundState { get { return _instructionsController.GetCurrentBackgroundState; } }
}