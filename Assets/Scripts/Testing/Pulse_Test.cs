﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Effects;
using Assets.Scripts.Models;

public class Pulse_Test : MonoBehaviour
{
    private MeshCollider meshCollider;
    private GameObject currenteffect;
    private Color currentEffectColor;
    private ResourceService _resourceService;

    //private bool spawnEffect;

    public void Awake()
    {
        meshCollider = GetComponent<MeshCollider>();

        _resourceService =  GetComponent<ResourceService>();

        //pawnEffect = true;// this is something we will need to update when we get this pulse worked out
    }

    private void Start()
    {
        UpdateEffectColor(Color.red);
        UpdateEffect(_resourceService.GetEffectByName("Pulse"));

        StartCoroutine(SpawnEffect());
    }

    public void UpdateEffectColor(Color color)
    {
        currentEffectColor = color;
    }

    public void UpdateEffect(GameObject effect)
    {
        currenteffect = effect;
    }

    public void UpdateState(bool IsVisible, bool IsActive, bool IsLaunchAnyway, bool IsTooManyPCs, bool IsPlayingGame)
    {
        var state = IsVisible && IsActive && !IsLaunchAnyway && !IsTooManyPCs && !IsPlayingGame;

        SetMeshColliderActive(state);

        if (state)
            StartCoroutine(SpawnEffect());
    }

    private void SetMeshColliderActive(bool status)
    {
        meshCollider.enabled = status;
    }

    IEnumerator SpawnEffect()
    {
        yield return new WaitForSecondsRealtime(.3f);

        Spawn();

        StartCoroutine(SpawnEffect());
    }

    private void Spawn()
    {
        var inst = Instantiate(currenteffect, transform);
        
        
        IEffect e = inst.GetComponentInChildren<IEffect>();

        e.SetModelState(new PulseModel()
        {
            Color = currentEffectColor,
            ScaleFactor = 1.5f,
            Duration = .5f,
            AlphaTo = 0f
        });

        e.Activate();
    }

    private void OnMouseUp()
    {
        Debug.Log("WE Will start the spawn now");
    }
}

