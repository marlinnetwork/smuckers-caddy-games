﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeBerries : MonoBehaviour {

	private GameObject[] berries;
	private Vector3 explosionPos;
	private List<Rigidbody> berryBodies;

	public float explosionRadius = 1.0f;
  public float explosionPower = 13.05f;

	public float upwardsModifier = 2.8f;

	// Use this for initialization
	void Start () {
		berries = GameObject.FindGameObjectsWithTag("Strawberry");

		explosionPos = transform.position;
	}

	private void OnMouseUp() {

		foreach(GameObject berry in berries)
		{
			var berryBody = berry.GetComponentInChildren<Rigidbody>();
			berryBody.AddExplosionForce(explosionPower, explosionPos, explosionRadius, upwardsModifier);
		}

	}

	// Update is called once per frame
	void Update () {
		
	}
}
