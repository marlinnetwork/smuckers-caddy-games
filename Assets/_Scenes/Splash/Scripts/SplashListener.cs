﻿using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;

public class SplashListener : MonoBehaviour {

	public GameObject selectedSplash;
	
	private Animator[] splashElements;

	private bool wasKeyPressed;

	private bool isSceneLoading;

	private float splashTime;

	[SerializeField]
	public float splashWaitTime = 4.0f;

	void Awake() {
		splashElements = selectedSplash.GetComponentsInChildren<Animator>();

		wasKeyPressed = false;
		isSceneLoading = false;
	}

	void Start() {
		splashTime = Time.time;
	}

	void OnEnable()
	{
		Lean.Touch.LeanTouch.OnFingerTap += OnFingerTap;
	}

	void OnDisable()
	{
		Lean.Touch.LeanTouch.OnFingerTap -= OnFingerTap;
	}

	void OnFingerTap(Lean.Touch.LeanFinger finger)
	{
		if(!wasKeyPressed) 
		{
			wasKeyPressed = true;
		}
	}
	void Update () {

		var elapsed = Time.time - splashTime;

		if(wasKeyPressed || (elapsed >= splashWaitTime))
		{
			FadeOutSplashElements();
			// Debug.Log("Time Elapsed: " + elapsed);
		}

		if(!isSceneLoading && IsAnimationFinished(splashElements[0], "FadeOut"))
		{
			TryLoadMainLevel();
		}
	}

	void FadeOutSplashElements()
	{
		foreach(Animator a in splashElements)
		{
			a.SetBool("isKeyPressed", true);
		}
	}

	void TryLoadMainLevel()
	{
		//Animation has finished.
		Initiate.Fade("Main", Color.black, 2.0f);
		Debug.Log("Switching to next scene.");
		isSceneLoading = true;
	}

	bool IsAnimationFinished(Animator animator, string animName)
	{
		return (animator.GetCurrentAnimatorStateInfo(0).IsName(animName) 
		&& animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1
		&& !(animator.IsInTransition(0)));	
	}
}
